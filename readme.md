## Automatic cross exchange arbitrage trader
Creates chains of profitable trades across exchanges optimising
for price differences and fees.

**Abandoned** : 
> While functional in theory, steps in the trade chain need to be executed in sequence.
Resulting time delays make this too risky for any real-world application.

> Most of supported exchange APIs were versioned out. 
