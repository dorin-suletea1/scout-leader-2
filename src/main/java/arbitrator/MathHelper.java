package arbitrator;

import java.util.List;

/**
 * Created by next on 12/17/17.
 */
public class MathHelper {
    public static double percentOf(final double percent, final double number) {
        return (number * percent) / 100;
    }

    public static double percentGain(final double first, final double second) {
        return first / second * 100;
    }

//    /**
//     * Calculates the weighted average of a map.
//     *
//     * @param map A map of values and weights
//     */
//    public static Double calculateWeightedAverage(Map<Double, Integer> map) throws ArithmeticException {
//        double num = 0;
//        double denominator = 0;
//        for (Map.Entry<Double, Integer> entry : map.entrySet()) {
//            num += entry.getKey() * entry.getValue();
//            denominator += entry.getValue();
//        }
//        return num / denominator;
//    }

    public static Double calculateWeightedAverage(List<Double> input) throws ArithmeticException {
        double num = 0;
        double denominator = 0;
        for (int i = 0; i < input.size(); i++) {
            num += input.get(i) * i;
            denominator += i;
        }
        return num / denominator;
    }


    public static int digitCount(long n) {
        if (n == 0) return 1;
        int l;
        n = Math.abs(n);
        for (l = 0; n > 0; ++l)
            n /= 10;
        return l;
    }
}
