package arbitrator.exchange;

import com.google.inject.AbstractModule;
import arbitrator.exchange.bindings.BinanceApi;
import arbitrator.exchange.bindings.BinanceApiImpl;
import arbitrator.exchange.bindings.KucoinApi;
import arbitrator.exchange.bindings.KucoinApiImpl;
import arbitrator.exchange.util.Clock;
import arbitrator.exchange.util.ClockImpl;
import arbitrator.exchange.util.KeyConfiguration;
import arbitrator.exchange.util.KeyConfigurationImpl;

public class ExchangeApiModule extends AbstractModule{
    @Override
    protected void configure() {
        bind(Clock.class).to(ClockImpl.class).asEagerSingleton();
        bind(KeyConfiguration.class).to(KeyConfigurationImpl.class);
        bind(BinanceApi.class).to(BinanceApiImpl.class);
        bind(KucoinApi.class).to(KucoinApiImpl.class);

        bind(BinanceManager.class).to(BinanceManagerImpl.class).asEagerSingleton();
        bind(KucoinManager.class).to(KucoinManagerImpl.class).asEagerSingleton();
    }
}
