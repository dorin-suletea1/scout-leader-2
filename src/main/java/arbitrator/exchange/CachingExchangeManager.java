package arbitrator.exchange;

import arbitrator.MathHelper;
import arbitrator.exchange.bindings.ExchangeApi;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.shared.NotEnoughCoinException;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.*;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class CachingExchangeManager implements ExchangeManager {
    private List<InstrumentAndPrice> instrumentsCache;
    private Map<String, CoinInfo> infoCache;
    private final ExchangeApi exchangeApi;

    public CachingExchangeManager(final ExchangeApi exchangeApi) {
        this.instrumentsCache = new ArrayList<>();
        this.infoCache = new HashMap<>();
        this.exchangeApi = exchangeApi;
    }

    @Override
    public void refreshCaches() {
        instrumentsCache.clear();
        infoCache.clear();
    }

    @Override
    public Exchange getExchange() {
        return exchangeApi.getExchange();
    }

    @Override
    public List<InstrumentAndPrice> getInstruments() {
        if (instrumentsCache.isEmpty()) {
            instrumentsCache = exchangeApi.getInstruments().stream().map(apiInstrument -> ExchangeManager.unpackApiInstrument(
                    apiInstrument,
                    getIBuyFee(apiInstrument),
                    getISellFee(apiInstrument),
                    this.getExchange())).collect(ArrayList::new, List::addAll, List::addAll);
        }
        return instrumentsCache;
    }

    @Override
    public Map<String, CoinInfo> getCoinInfo() {
        if (infoCache.isEmpty()) {
            infoCache = exchangeApi.getCoinInfos().stream().collect(Collectors.toMap(coinInfo -> coinInfo.getCoin(), coinInfo -> coinInfo));
        }
        return infoCache;
    }

    @Override
    public String getDepositAddress(final String coin) {
        return exchangeApi.getDepositAddress(coin);
    }

    @Override
    public void sendCoinToAddress(final CoinAndCount coinAndVolumeToSend, final String address) {
        CoinAndCount balanceForCoin = this.getBalanceForCoin(coinAndVolumeToSend.getCoin());
        if (balanceForCoin.getCoinCount() < coinAndVolumeToSend.getCoinCount()) {
            throw new NotEnoughCoinException("Trying to send " + coinAndVolumeToSend.getCoinCount() + " but have " + balanceForCoin.getCoinCount() + " of " + coinAndVolumeToSend.getCoin());
        }
        exchangeApi.sendCoinToAddress(coinAndVolumeToSend.getCoin(), address, coinAndVolumeToSend.getCoinCount());
    }

    @Override
    public CoinAndCount getBalanceForCoin(final String coin) {
        List<CoinAndCount> assets = exchangeApi.getAssets();
        for (CoinAndCount asset : assets) {
            if (asset.getCoin().equals(coin)) {
                return new CoinAndCount(asset.getCoin(), asset.getCoinCount());
            }
        }
        return new CoinAndCount(coin, 0d);
    }

    @Override
    public CoinAndCount getOrderExactOutput(final Instrument instrument, final CoinAndCount input) throws NotEnoughVolumeException {
        final String symbol = this.getSymbolFromInstrument(instrument);
        final List<ApiOrderBook.PriceAndVolume> entries = exchangeApi.getOrderBook(symbol).getSortedOrders(instrument.getSide());

        //TODO : commission is wrong, if it's flat subtract once per transaction ; if variable calculate volume in 'coins spent' and extract fees before-hand.
        final double commission = MathHelper.percentOf(getExchange().getExchangeFee(), input.getCoinCount());
        double remainingMyCoinCount = input.getCoinCount() - commission;
        double totalCoinsGained = 0;

        for (ApiOrderBook.PriceAndVolume entry : entries) {
            double volumeToExecute = getVolumeToExecute(instrument.getSide(), remainingMyCoinCount, entry);
            double stepPrice = entry.getPrice() * volumeToExecute;

            if (instrument.getSide() == Side.BUY) {
                totalCoinsGained += volumeToExecute;
                remainingMyCoinCount -= stepPrice;
            } else if (instrument.getSide() == Side.SELL) {
                remainingMyCoinCount -= volumeToExecute;
                totalCoinsGained += stepPrice;
            }
            if (remainingMyCoinCount <= 0) {
                break;
            }
        }
        if (remainingMyCoinCount > 0) {
            throw new NotEnoughVolumeException();
        }
        return new CoinAndCount(instrument.getSide() == Side.SELL ? instrument.getLeftCoin() : instrument.getRightCoin(), totalCoinsGained);
    }

    @Override
    public List<KLineData> getChronologicallySortedKLineData(final Instrument instrument, final int intervalMinutes, final int dataLimit) {
        final String symbol = this.getSymbolFromInstrument(instrument);
        List<KLineData> ret = exchangeApi.getKLineData(symbol, intervalMinutes, dataLimit);
        ret.sort((o1, o2) -> (int) (o1.getCandleOpenTs() - o2.getCandleOpenTs()));
        return ret;
    }

    @Override
    public Pair<String, Double> runOrder(final Instrument instrument, final CoinAndCount input) {
        final String symbol = this.getSymbolFromInstrument(instrument);
        ApiOrderBook.PriceAndVolume orderBookEdge = exchangeApi.getOrderBook(symbol).getSortedOrders(instrument.getSide()).get(0);

        return exchangeApi.submitLimitOrder(
                symbol,
                orderBookEdge.getPrice() * input.getCoinCount(),
                orderBookEdge.getPrice(),
                instrument.getSide());
    }

    @Override
    public boolean hasActiveOrder(final Instrument instrument, final String orderId) {
        return exchangeApi.getMyActiveOrders(this.getSymbolFromInstrument(instrument)).stream().anyMatch(apiOrderInfo -> apiOrderInfo.getOrderId().equals(orderId));
    }

    private double getVolumeToExecute(final Side tradeSide, final double inputCoinCount, ApiOrderBook.PriceAndVolume entry) {
        if (tradeSide == Side.BUY) {
            return Math.min(entry.getVolume(), inputCoinCount / entry.getPrice());
        }
        if (tradeSide == Side.SELL) {
            return Math.min(entry.getVolume(), inputCoinCount);
        }

        throw new RuntimeException("Unreachable");
    }

    //
//    /**
//     * 1 = 100% will execute
//     * 0 = 0%   will execute
//     * Full Confidence (1) == 50 or 30 min ticks have
//     * volume = 10 * my volume                     = 1  * weight = 50%
//     * price  = is trending up                     = 1  * weight = 30% the smaller the average PVT the better(Volume Price Trend Indicator) PVT = [((CurrentClose - PreviousClose) / PreviousClose) x Volume] + PreviousPVT
//     * my price is not 20% higher than the average = 1  * weight = 20%
//     **/
//    @Override
//    public double getOrderExecutesIn30MinConfidenceFactor(final InstrumentAndPrice instrument, final CoinAndCount input) {
//        final int volumeWeight = 50;
//        final int priceTrendingWeight = 30;
//        final int priceFitsAverage = 20;
//        final String symbol = this.getSymbolFromInstrument(instrument.getInstrument());
//        final double myVolume = instrument.getInstrument().getSide() == Side.BUY ? input.getCoinCount() * instrument.getPrice() : input.getCoinCount();
//
//        //TODO : this needs to be tested and maybe extracted
//        List<KLineData> kLines = exchangeApi.getChronologicallySortedKLineData(symbol, Constants.KLINE_DATA_INTERVAL_MINUTES, Constants.KLINE_DATA_LIMIT);
//        kLines.sort((o1, o2) -> (int) (o1.getCandleOpenTs() - o2.getCandleOpenTs()));
//        double weightedAveragePrice = getWeightedAveragePrice(kLines);
//
//        double priceTrendingUpFactor = getWeightedAveragePriceVolumeIndicator(kLines) * priceTrendingWeight;
//        double priceFitsAverageFactor = (MathHelper.percentGain(instrument.getPrice(), weightedAveragePrice) < 20 ? 1 : 0) * priceFitsAverage;
//        double volumeFitFactor = getWeightedDoesVolumeFit(kLines, myVolume * 10) * volumeWeight;
//
//
//        //can not be higher than 1
//        return priceTrendingUpFactor + priceFitsAverageFactor + volumeFitFactor;
//    }

}
