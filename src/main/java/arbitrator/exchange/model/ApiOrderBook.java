package arbitrator.exchange.model;

import arbitrator.shared.model.Side;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ApiOrderBook {
    /**Bids: demand ; I sell*/
    /**Asks: offer  ; I Buy*/
    private final List<PriceAndVolume> marketDemand;
    private final List<PriceAndVolume> marketOffer;

    public ApiOrderBook(final List<PriceAndVolume> demand, final List<PriceAndVolume> offer) {
        this.marketDemand = demand;
        this.marketOffer = offer;
    }

    public List<PriceAndVolume> getSortedOrders(final Side myTradeSide) {
        switch (myTradeSide) {
            case BUY:
                List<PriceAndVolume> sortedMarketOffer = new ArrayList<>(getMarketOffer());
                sortedMarketOffer.sort(PriceAndVolume.getDescendingPriceOrderComparator());
                return Collections.unmodifiableList(sortedMarketOffer);
            case SELL:
                List<PriceAndVolume> sortedMarketDemand= new ArrayList<>(getMarketDemand());
                sortedMarketDemand.sort(PriceAndVolume.getAscendingPriceOrderComparator());
                return Collections.unmodifiableList(sortedMarketDemand);
        }
        return null;
    }

    public List<PriceAndVolume> getMarketOffer() {
        return Collections.unmodifiableList(marketOffer);
    }

    public List<PriceAndVolume> getMarketDemand() {
        return Collections.unmodifiableList(marketDemand);
    }

    public static class PriceAndVolume {
        private final double price;
        private final double volume;

        public PriceAndVolume(final double price, final double volume) {
            this.price = price;
            this.volume = volume;
        }

        @Override
        public String toString() {
            return "PriceAndVolume{" +
                    "price=" + price +
                    ", volume=" + volume +
                    '}';
        }

        public double getPrice() {
            return price;
        }

        public double getVolume() {
            return volume;
        }

        public static Comparator<PriceAndVolume> getAscendingPriceOrderComparator() {
            return (o1, o2) -> {
                if (o1.getPrice() > o2.getPrice()) {
                    return -1;
                }
                if (o1.getPrice() < o2.getPrice()) {
                    return 1;
                }
                return 0;
            };
        }

        public static Comparator<PriceAndVolume> getDescendingPriceOrderComparator() {
            return (o1, o2) -> {
                if (o1.getPrice() > o2.getPrice()) {
                    return 1;
                }
                if (o1.getPrice() < o2.getPrice()) {
                    return -1;
                }
                return 0;
            };
        }

    }

    @Override
    public String toString() {
        return "ApiOrderBook{" +
                "demand=" + marketDemand +
                ", offer=" + marketOffer +
                '}';
    }

}
