package arbitrator.exchange.model;

public class ApiInstrument {
    private final String leftCoin;
    private final String rightCoin;
    private final double iBuyPriceNoFee;
    private final double iSellPriceNoFee;

    public ApiInstrument(final String leftCoin, final String rightCoin, final double iBuyPriceNoFee, final double iSellPriceNoFee) {
        this.leftCoin = leftCoin;
        this.rightCoin = rightCoin;
        this.iBuyPriceNoFee = iBuyPriceNoFee;
        this.iSellPriceNoFee = iSellPriceNoFee;
    }

    public String getLeftCoin() {
        return leftCoin;
    }

    public String getRightCoin() {
        return rightCoin;
    }

    public Double getIBuyPriceNoFee() {
        return iBuyPriceNoFee;
    }

    public Double getISellPriceNoFee() {
        return iSellPriceNoFee;
    }

    @Override
    public String toString() {
        return "ApiInstrument{" +
                "leftCoin='" + leftCoin + '\'' +
                ", rightCoin='" + rightCoin + '\'' +
                ", iBuyPriceNoFee=" + iBuyPriceNoFee +
                ", iSellPriceNoFee=" + iSellPriceNoFee +
                '}';
    }
}
