package arbitrator.exchange.model;

import arbitrator.shared.model.Side;

public class ApiOrderInfo {
    private final String symbol;
    private final String orderId;
    private final double price;
    private final double initialQuantity;
    private final double executedQuantity;
    private final Side side;

    public ApiOrderInfo(final String symbol, final String orderId, final double price, final double initialQuantity, final double executedQuantity, final Side side) {
        this.symbol = symbol;
        this.orderId = orderId;
        this.price = price;
        this.initialQuantity = initialQuantity;
        this.executedQuantity = executedQuantity;
        this.side = side;
    }

    public String getOrderId() {
        return orderId;
    }

    public double getInitialQuantity() {
        return initialQuantity;
    }
}
