package arbitrator.exchange;

import arbitrator.exchange.bindings.KucoinApi;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.Instrument;
import arbitrator.shared.model.Side;

import javax.inject.Inject;

public class KucoinManagerImpl extends CachingExchangeManager implements KucoinManager {
    @Inject
    public KucoinManagerImpl(final KucoinApi exchangeApi) {
        super(exchangeApi);
    }

    @Override
    public String getSymbolFromInstrument(final Instrument instrument) {
        final String rightPart = instrument.getRightCoin();
        final String leftPart = instrument.getLeftCoin();
        return instrument.getSide() == Side.SELL ? leftPart + "-" + rightPart : rightPart + "-" + leftPart;
    }

    @Override
    public Exchange getExchange() {
        return Exchange.KUCOIN;
    }
}
