package arbitrator.exchange;

import arbitrator.MathHelper;
import arbitrator.exchange.model.ApiInstrument;
import arbitrator.shared.model.KLineData;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.*;
import javafx.util.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public interface ExchangeManager {
    /**
     * GET
     */
    void refreshCaches();
    String getSymbolFromInstrument(final Instrument instrument);
    Exchange getExchange();
    List<InstrumentAndPrice> getInstruments();
    Map<String, CoinInfo> getCoinInfo();

    List<KLineData> getChronologicallySortedKLineData(final Instrument instrument, final int intervalMinutes, final int dataLimit);
    CoinAndCount getOrderExactOutput(final Instrument instrument, final CoinAndCount input) throws NotEnoughVolumeException;
    /**
     * POST
     */
    String getDepositAddress(final String coin);
    void sendCoinToAddress(final CoinAndCount coinAndVolumeToSend, final String address);
    CoinAndCount getBalanceForCoin(final String coin);
    Pair<String,Double> runOrder(final Instrument instrument, final CoinAndCount input);
    boolean hasActiveOrder(final Instrument instrument, final String orderId);



    static List<InstrumentAndPrice> unpackApiInstrument(final ApiInstrument apiInstrument,
                                                        final double iBuyTradeFee,
                                                        final double iSellTradeFee,
                                                        final Exchange exchange) {
        final Double iBuyPrice = apiInstrument.getIBuyPriceNoFee() + iBuyTradeFee;
        final Double iSellPrice = apiInstrument.getISellPriceNoFee() - iSellTradeFee;
        final InstrumentAndPrice buyInstrument = new InstrumentAndPrice(new Instrument(apiInstrument.getLeftCoin(), apiInstrument.getRightCoin(), Side.BUY, exchange), 1 / iBuyPrice);
        final InstrumentAndPrice sellInstrument = new InstrumentAndPrice(new Instrument(apiInstrument.getRightCoin(), apiInstrument.getLeftCoin(), Side.SELL, exchange), iSellPrice);
        return Arrays.asList(buyInstrument, sellInstrument);
    }

    default double getIBuyFee(final ApiInstrument apiInstrument) {
        return MathHelper.percentOf(this.getExchange().getExchangeFee(), apiInstrument.getIBuyPriceNoFee());
    }

    default double getISellFee(final ApiInstrument apiInstrument) {
        return MathHelper.percentOf(this.getExchange().getExchangeFee(), apiInstrument.getISellPriceNoFee());
    }
}

