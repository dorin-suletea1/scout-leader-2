package arbitrator.exchange;

import arbitrator.exchange.bindings.BinanceApi;
import arbitrator.shared.model.Instrument;
import arbitrator.shared.model.Side;

import javax.inject.Inject;

public class BinanceManagerImpl extends CachingExchangeManager implements BinanceManager {

    @Inject
    public BinanceManagerImpl(final BinanceApi exchangeApi) {
        super(exchangeApi);
    }

    @Override
    public String getSymbolFromInstrument(final Instrument instrument) {
        final String rightPart = instrument.getRightCoin();
        final String leftPart = instrument.getLeftCoin();
        return instrument.getSide() == Side.SELL ? leftPart + rightPart : rightPart + leftPart;
    }
}
