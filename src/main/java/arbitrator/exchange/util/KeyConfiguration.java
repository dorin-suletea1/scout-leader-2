package arbitrator.exchange.util;

import arbitrator.shared.model.Exchange;

public interface KeyConfiguration {
    String getApiKey(final Exchange exchange);
    String getApiSecret(final Exchange exchange);
}
