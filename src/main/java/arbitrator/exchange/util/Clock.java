package arbitrator.exchange.util;

public interface Clock {
    void synchronize(long tsInMillis);
    long getCurrentMillis();
}
