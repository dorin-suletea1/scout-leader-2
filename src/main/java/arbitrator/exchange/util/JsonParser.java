package arbitrator.exchange.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

    public static JSONArray getJsonArray(final String str) {
        try {
            return new JSONArray(str);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static JSONObject getJsonObject(final String str) {
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static JSONObject getFromArray(final JSONArray array, final int index) {
        try {
            return array.getJSONObject(index);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static JSONArray getArrayFromArray(final JSONArray array, final int index) {
        try {
            return array.getJSONArray(index);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static double getDoubleFromArray(final JSONArray array, final int index) {
        try {
            return array.getDouble(index);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static long getLongFromArray(final JSONArray array, final int index) {
        try {
            return array.getLong(index);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static JSONObject getJsonObject(final JSONObject object, final String tag) {
        try {
            return object.getJSONObject(tag);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    public static String getString(final JSONObject object, final String tag) {
        try {
            return object.getString(tag);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static double getDouble(final JSONObject object, final String tag) {
        try {
            return object.getDouble(tag);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static long getLong(final JSONObject object, final String tag) {
        try {
            return object.getLong(tag);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static JSONArray getJsonArray(final JSONObject object, final String tag) {
        try {
            return object.getJSONArray(tag);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean getBoolean(final JSONObject object, final String tag) {
        try {
            return object.getBoolean(tag);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
