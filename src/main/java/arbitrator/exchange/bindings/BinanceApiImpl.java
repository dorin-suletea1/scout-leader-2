package arbitrator.exchange.bindings;

import arbitrator.exchange.model.ApiInstrument;
import arbitrator.shared.model.KLineData;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.exchange.model.ApiOrderInfo;
import arbitrator.exchange.util.Clock;
import arbitrator.exchange.util.HttpDispatcher;
import arbitrator.exchange.util.JsonParser;
import arbitrator.exchange.util.KeyConfiguration;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.CoinInfo;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.Side;
import javafx.util.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://github.com/binance-exchange/binance-official-api-docs/blob/master/wapi-api.md
 */
public class BinanceApiImpl implements BinanceApi {
    private static long RECEIVE_WINDOW = 7000;
    private static long ORDER_BOOK_FETCH_LIMIT = 50;
    private static String SIDE_BUY = "BUY";
    private static String SIDE_SELL = "SELL";

    private final Provider<KeyConfiguration> keyConfigurationProvider;
    private final Clock clock;

    @Inject
    public BinanceApiImpl(final Provider<KeyConfiguration> keyConfigurationProvider, final Clock clock) {
        this.keyConfigurationProvider = keyConfigurationProvider;
        this.clock = clock;
        this.clock.synchronize(getServerTimestamp());
    }

    @Override
    public Exchange getExchange() {
        return Exchange.BINANCE;
    }

    @Override
    public List<ApiInstrument> getInstruments() {
        final List<ApiInstrument> ret = new ArrayList<>();
        String apiResponse = HttpDispatcher.sendGetWithSSLCert(BinanceApi.INSTRUMENTS_URL);

        JSONArray results = JsonParser.getJsonArray(apiResponse);
        for (int i = 0; i < results.length(); i++) {
            JSONObject instrumentData = JsonParser.getFromArray(results, i);
            String symbols = JsonParser.getString(instrumentData, "symbol");
            //on binance pairs are reversed, and we use the fact that all of the pairs you can buy for have 3 letters (ETH,BTC,BNB,USDT)
            final String rightSymbol = symbols.substring(0, symbols.length() - 3);
            final String leftSymbol = symbols.substring(symbols.length() - 3, symbols.length());
            final double iBuyPriceNoFee = JsonParser.getDouble(instrumentData, "askPrice");
            final double iSellPriceNoFee = JsonParser.getDouble(instrumentData, "bidPrice");

            ApiInstrument newPair = new ApiInstrument(leftSymbol, rightSymbol, iBuyPriceNoFee, iSellPriceNoFee);
            ret.add(newPair);
        }
        return ret;
    }

    @Override
    public List<CoinInfo> getCoinInfos() {
        List<CoinInfo> ret = new ArrayList<>();
        final String apiResponse = HttpDispatcher.sendGet(BinanceApi.COIN_INFO_URL);
        JSONArray results = JsonParser.getJsonArray(apiResponse);

        for (int i = 0; i < results.length(); i++) {
            JSONObject instrumentInfo = JsonParser.getFromArray(results, i);
            final String coin = JsonParser.getString(instrumentInfo, "assetCode");
            final double txFee = JsonParser.getDouble(instrumentInfo, "transactionFee");
            final boolean depositEnabled = JsonParser.getBoolean(instrumentInfo, "enableCharge");
            final boolean withdrawEnabled = JsonParser.getBoolean(instrumentInfo, "enableWithdraw");

            CoinInfo info = new CoinInfo(coin, txFee, depositEnabled && withdrawEnabled, getExchange());
            ret.add(info);
        }
        return ret;

    }

    @Override
    public List<KLineData> getKLineData(final String symbol, final int interval, final int limit) {
        List<KLineData> ret = new ArrayList<>();
        final String queryString = "symbol=" + symbol + "&interval=" + interval + "m" + "&limit=" + limit;
        final String requestUrl = BinanceApi.KLINE_URL + "?" + queryString;
        final String apiResponse = HttpDispatcher.sendGet(requestUrl);
        JSONArray results = JsonParser.getJsonArray(apiResponse);

        for (int i = 0; i < results.length(); i++) {
            JSONArray klineItem = JsonParser.getArrayFromArray(results, i);

            final long candleOpenTs = JsonParser.getLongFromArray(klineItem, 0);
            final double highPrice = JsonParser.getDoubleFromArray(klineItem, 2);
            final double lowPrice = JsonParser.getDoubleFromArray(klineItem, 3);
            final double candleClosePrice = JsonParser.getDoubleFromArray(klineItem, 4);
            final double volume = JsonParser.getDoubleFromArray(klineItem, 5);


            KLineData dataItem = new KLineData(volume, highPrice, lowPrice, candleOpenTs, candleClosePrice);
            ret.add(dataItem);
        }
        return ret;
    }

    @Override
    public List<ApiOrderInfo> getMyActiveOrders(final String symbol) {
        List<ApiOrderInfo> ret = new ArrayList<>();
        final String requestUrl = BinanceApi.ACTIVE_ORDERS;
        final String queryString = "symbol=" + symbol + "&" + makeTimeStampParameters();
        final String signature = "&" + makeSignatureParameters(queryString);
        final String response = HttpDispatcher.sendGetWithSSLCert(requestUrl + "?" + queryString + signature, authenticationHeaders());

        JSONArray ordersJson = JsonParser.getJsonArray(response);
        for (int i = 0; i < ordersJson.length(); i++) {
            JSONObject orderJson = JsonParser.getFromArray(ordersJson, i);

            final String receivedSymbol = JsonParser.getString(orderJson, "symbol");
            final String id = JsonParser.getString(orderJson, "orderId");
            final double price = JsonParser.getDouble(orderJson, "price");
            final double initialQuantity = JsonParser.getDouble(orderJson, "origQty");
            final double executedQuantity = JsonParser.getDouble(orderJson, "executedQty");
            final Side side = Side.fromString(JsonParser.getString(orderJson, "side"));

            ret.add(new ApiOrderInfo(receivedSymbol, id, price, initialQuantity, executedQuantity, side));
        }
        return ret;
    }

    @Override
    public List<CoinAndCount> getAssets() {
        List<CoinAndCount> ret = new ArrayList<>();
        final String requestUrl = BinanceApi.ASSETS_URL;
        final String queryString = makeTimeStampParameters();
        final String signature = "&" + makeSignatureParameters(queryString);
        final String response = HttpDispatcher.sendGetWithSSLCert(requestUrl + "?" + queryString + signature, authenticationHeaders());
        JSONObject assetsObject = JsonParser.getJsonObject(response);
        JSONArray assetsArray = JsonParser.getJsonArray(JsonParser.getString(assetsObject, "balances"));
        for (int i = 0; i < assetsArray.length(); i++) {
            JSONObject assetObject = JsonParser.getFromArray(assetsArray, i);
            ret.add(new CoinAndCount(
                    JsonParser.getString(assetObject, "asset"),
                    JsonParser.getDouble(assetObject, "free")
            ));
        }
        return ret;
    }

    @Override
    public String getDepositAddress(final String coin) {
        final String queryString = "asset=" + coin + "&" + makeTimeStampParameters();
        final String signature = makeSignatureParameters(queryString);
        final String requestUrl = BinanceApi.DEPOSIT_API + "?" + queryString + "&" + signature;
        final String response = HttpDispatcher.sendGetWithSSLCert(requestUrl, authenticationHeaders());
        return JsonParser.getString(JsonParser.getJsonObject(response), "address");
    }

    @Override
    public void sendCoinToAddress(final String coin, final String walletAddress, final double volume) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public ApiOrderBook getOrderBook(final String symbol) {
        List<ApiOrderBook.PriceAndVolume> demand = new ArrayList<>();
        List<ApiOrderBook.PriceAndVolume> offer = new ArrayList<>();
        final String queryString = "?symbol=" + symbol + "&limit=" + ORDER_BOOK_FETCH_LIMIT;
        final String requestUrl = BinanceApi.ORDER_BOOK + queryString;
        final String response = HttpDispatcher.sendGet(requestUrl);

        JSONObject responseJson = JsonParser.getJsonObject(response);
        JSONArray demandJson = JsonParser.getJsonArray(responseJson, "bids");
        for (int i = 0; i < demandJson.length(); i++) {
            JSONArray bidJson = JsonParser.getArrayFromArray(demandJson, i);
            final double bidPrice = JsonParser.getDoubleFromArray(bidJson, 0);
            final double bidVolume = JsonParser.getDoubleFromArray(bidJson, 1);
            demand.add(new ApiOrderBook.PriceAndVolume(bidPrice, bidVolume));
        }

        JSONArray offerJson = JsonParser.getJsonArray(responseJson, "asks");
        for (int i = 0; i < offerJson.length(); i++) {
            JSONArray askJson = JsonParser.getArrayFromArray(offerJson, i);
            final double askPrice = JsonParser.getDoubleFromArray(askJson, 0);
            final double askVolume = JsonParser.getDoubleFromArray(askJson, 1);
            offer.add(new ApiOrderBook.PriceAndVolume(askPrice, askVolume));
        }
        return new ApiOrderBook(demand, offer);
    }

    @Override
    public Pair<String, Double> submitLimitOrder(final String symbol, final double volume, final double price, final Side side) {
        switch (side) {
            case BUY:
                return makeLimitOrder(symbol, volume, price, SIDE_BUY);
            case SELL:
                return makeLimitOrder(symbol, volume, price, SIDE_SELL);
        }
        throw new RuntimeException("Unreachable");
    }

    @Deprecated
    public void cancelOrder(final String symbol, final long orderId) throws RuntimeException {
        final String queryString = "symbol=" + symbol + "&orderId=" + orderId + "&" + makeTimeStampParameters();
        final String signature = makeSignatureParameters(queryString);
        final String requestUrl = BinanceApi.CANCEL_ORDER + "?" + queryString + "&" + signature;

        try {
            HttpDispatcher.sendDeleteWithSSLCert(requestUrl, authenticationHeaders());
        } catch (RuntimeException e) {
            throw e;
        }
    }

    private List<Pair<String, String>> authenticationHeaders() {
        return Arrays.asList(new Pair[]{
                new Pair("X-MBX-APIKEY", keyConfigurationProvider.get().getApiKey(Exchange.BINANCE)),
                new Pair("Content-Type", "application/x-www-form-urlencoded")
        });
    }

    private long getServerTimestamp() {
        try {
            final String apiResponse = HttpDispatcher.sendGetWithSSLCert(BinanceApi.TIME_URL);
            final JSONObject timeObject = new JSONObject(apiResponse);
            final long serverTime = timeObject.getLong("serverTime");
            return serverTime;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String makeSignatureParameters(final String queryString) {
        return "signature=" + HttpDispatcher.encodeHmac(keyConfigurationProvider.get().getApiSecret(Exchange.BINANCE), queryString);
    }

    private String makeTimeStampParameters() {
        return "recvWindow=" + RECEIVE_WINDOW + "&timestamp=" + clock.getCurrentMillis();
    }

    private Pair<String, Double> makeLimitOrder(final String symbol, final double volume, final double price, final String side) {
        final String orderType = "LIMIT";
        final String orderExpiration = "GTC";
        final String queryString = "symbol=" + symbol + "&side=" + side + "&type=" + orderType +
                "&timeInForce=" + orderExpiration + "&quantity=" + volume + "&price=" + price + "&" + makeTimeStampParameters();
        final String signature = makeSignatureParameters(queryString);
        final String requestUrl = BinanceApi.MAKE_ODER + "?" + queryString + "&" + signature;
        final String response = HttpDispatcher.sendPostWithSSLCert(requestUrl, authenticationHeaders());
        final String orderId = JsonParser.getString(JsonParser.getJsonObject(response), "orderId");
        final double originalQuantity = JsonParser.getDouble(JsonParser.getJsonObject(response), "origQty");
        return new Pair(orderId, originalQuantity);
    }
}