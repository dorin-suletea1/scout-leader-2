package arbitrator.exchange.bindings;

import arbitrator.exchange.model.ApiInstrument;
import arbitrator.shared.model.KLineData;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.exchange.model.ApiOrderInfo;
import arbitrator.exchange.util.Clock;
import arbitrator.exchange.util.HttpDispatcher;
import arbitrator.exchange.util.JsonParser;
import arbitrator.exchange.util.KeyConfiguration;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.CoinInfo;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.Side;
import javafx.util.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class KucoinApiImpl implements KucoinApi {
    private final Provider<KeyConfiguration> keyConfigurationProvider;
    private final Clock clock;

    @Inject
    public KucoinApiImpl(final Provider<KeyConfiguration> keyConfigurationProvider, final Clock clock) {
        this.keyConfigurationProvider = keyConfigurationProvider;
        this.clock = clock;
        clock.synchronize(getServerTimestamp());
    }

    @Override
    public Exchange getExchange() {
        return Exchange.KUCOIN;
    }

    @Override
    public List<ApiInstrument> getInstruments() {
        final List<ApiInstrument> ret = new ArrayList<>();
        String apiResponse = HttpDispatcher.sendGet(KucoinApi.INSTRUMENTS_URL);
        JSONArray results = JsonParser.getJsonArray(JsonParser.getJsonObject(apiResponse), "data");

        for (int i = 0; i < results.length(); i++) {
            JSONObject instrumentData = JsonParser.getFromArray(results, i);
            if (!isValidJsonForInstrument(instrumentData)) {
                continue;
            }
            final String[] symbols = JsonParser.getString(instrumentData, "symbol").split("-");
            final String leftSymbol = symbols[1];
            final String rightSymbol = symbols[0];
            final double iBuyPriceNoFee = JsonParser.getDouble(instrumentData, "buy");
            final double iSellPriceNoFee = JsonParser.getDouble(instrumentData, "sell");

            ApiInstrument newPair = new ApiInstrument(leftSymbol, rightSymbol, iBuyPriceNoFee, iSellPriceNoFee);
            ret.add(newPair);
        }
        return ret;
    }


    @Override
    public List<CoinInfo> getCoinInfos() {
        List<CoinInfo> ret = new ArrayList<>();
        final String apiResponse = HttpDispatcher.sendGet(KucoinApi.COIN_INFO_URL);
        JSONArray results = JsonParser.getJsonArray(JsonParser.getJsonObject(apiResponse), "data");

        for (int i = 0; i < results.length(); i++) {
            JSONObject instrumentInfo = JsonParser.getFromArray(results, i);
            final String coin = JsonParser.getString(instrumentInfo, "coin");
            final double txFee = JsonParser.getDouble(instrumentInfo, "withdrawMinFee");
            final boolean depositEnabled = JsonParser.getBoolean(instrumentInfo, "enableDeposit");
            final boolean withdrawEnabled = JsonParser.getBoolean(instrumentInfo, "enableWithdraw");

            CoinInfo info = new CoinInfo(coin, txFee, depositEnabled && withdrawEnabled, getExchange());
            ret.add(info);
        }
        return ret;
    }

    @Override
    public List<KLineData> getKLineData(final String symbol, final int interval, final int limit) {
        return Collections.emptyList();
//        throw new RuntimeException("not implemented");
    }

    @Override
    public List<ApiOrderInfo> getMyActiveOrders(final String symbol) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public List<CoinAndCount> getAssets() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getDepositAddress(final String coin) {
        final String requestUrl = KucoinApi.DEPOSIT_API.replace("{coin}", coin);
        final String response = HttpDispatcher.sendGetWithSSLCert(requestUrl, authenticationHeaders(requestUrl));
        final JSONObject responseJson = JsonParser.getJsonObject(response);
        return JsonParser.getString(JsonParser.getJsonObject(responseJson, "data"), "address");
    }

    @Override
    public void sendCoinToAddress(final String coin, final String walletAddress, final double volume) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public ApiOrderBook getOrderBook(final String symbol) {
        List<ApiOrderBook.PriceAndVolume> demand = new ArrayList<>();
        List<ApiOrderBook.PriceAndVolume> offer = new ArrayList<>();

        final String requestUrl = KucoinApi.ORDER_BOOK.replace("{symbol}", symbol);
        final String response = HttpDispatcher.sendGet(requestUrl);
        final JSONObject responseJson = JsonParser.getJsonObject(JsonParser.getJsonObject(response), "data");

        JSONArray demandJson = JsonParser.getJsonArray(responseJson, "BUY");
        for (int i = 0; i < demandJson.length(); i++) {
            JSONArray bidJson = JsonParser.getArrayFromArray(demandJson, i);
            final double bidPrice = JsonParser.getDoubleFromArray(bidJson, 0);
            final double bidVolume = JsonParser.getDoubleFromArray(bidJson, 1);
            demand.add(new ApiOrderBook.PriceAndVolume(bidPrice, bidVolume));
        }

        JSONArray offerJson = JsonParser.getJsonArray(responseJson, "SELL");
        for (int i = 0; i < offerJson.length(); i++) {
            JSONArray askJson = JsonParser.getArrayFromArray(offerJson, i);
            final double askPrice = JsonParser.getDoubleFromArray(askJson, 0);
            final double askVolume = JsonParser.getDoubleFromArray(askJson, 1);
            offer.add(new ApiOrderBook.PriceAndVolume(askPrice, askVolume));
        }
        return new ApiOrderBook(demand, offer);
    }

    @Override
    public Pair<String, Double> submitLimitOrder(final String symbol, final double volume, final double price, final Side side) {
        throw new RuntimeException("Not implemented");
    }

    private boolean isValidJsonForInstrument(final JSONObject instrumentData) {
        AtomicInteger mandatoryParametersMissing = new AtomicInteger(3);
        instrumentData.keys().forEachRemaining(o -> {
            if (o instanceof String) {
                String keyName = ((String) o);
                if (keyName.equals("symbol") || keyName.equals("buy") || keyName.equals("sell")) {
                    mandatoryParametersMissing.decrementAndGet();
                }
            }
        });
        return mandatoryParametersMissing.get() == 0;
    }

    /**
     * Using binance server time, since Kucoin does not have a time endpoint
     */
    private long getServerTimestamp() {
        try {
            final String apiResponse = HttpDispatcher.sendGetWithSSLCert(BinanceApi.TIME_URL);
            final JSONObject timeObject = new JSONObject(apiResponse);
            final long serverTime = timeObject.getLong("serverTime");
            return serverTime;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<Pair<String, String>> authenticationHeaders(final String requestString) {
        try {
            final URL requestUrl = new URL(requestString);
            final String path = requestUrl.getPath();
            final String secret = keyConfigurationProvider.get().getApiKey(Exchange.KUCOIN);
            final String secretKey = keyConfigurationProvider.get().getApiSecret(Exchange.KUCOIN);
            final String nonce = clock.getCurrentMillis() + "";
            final String queryString = requestUrl.getQuery() != null ? requestUrl.getQuery() : "";
            final String strForSign = path + "/" + nonce + "/" + queryString;
            final String signatureStr = Base64.getEncoder().encodeToString(strForSign.getBytes("UTF-8"));
            final String signature = HttpDispatcher.encodeHmac(secretKey, signatureStr);
            return Arrays.asList(new Pair[]{
                    new Pair("KC-API-KEY", secret),
                    new Pair("KC-API-NONCE", nonce),
                    new Pair("KC-API-SIGNATURE", signature)
            });
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
