package arbitrator.exchange.bindings;

/*
 * https://github.com/binance-exchange/binance-official-api-docs/blob/master/wapi-api.md
 */
public interface BinanceApi extends ExchangeApi {
    String BINANCE_URL = "https://www.binance.com";
    String INSTRUMENTS_URL = BINANCE_URL + "/api/v1/ticker/allBookTickers";
    String KLINE_URL = BINANCE_URL+"/api/v1/klines";
    String TIME_URL = BINANCE_URL + "/api/v1/time";
    String ASSETS_URL = BINANCE_URL + "/api/v3/account";
    String DEPOSIT_API = "https://api.binance.com/wapi/v3/depositAddress.html";
    String COIN_INFO_URL = BINANCE_URL + "/assetWithdraw/getAllAsset.html";
    String ORDER_BOOK = BINANCE_URL + "/api/v1/depth";
    String ACTIVE_ORDERS = BINANCE_URL + "/api/v3/openOrders";
    String MAKE_ODER = BINANCE_URL + "/api/v3/order/test";
    String CANCEL_ORDER = BINANCE_URL + "/api/v3/order";


}
