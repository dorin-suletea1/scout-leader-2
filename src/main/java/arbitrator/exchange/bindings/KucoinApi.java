package arbitrator.exchange.bindings;

/*
 * https://github.com/webcerebrium/java-kucoin-api/
 * https://kucoinapidocs.docs.apiary.io/#
 */
public interface KucoinApi extends ExchangeApi {
    String KUCOIN_URL = "https://api.kucoin.com";
    String INSTRUMENTS_URL = KUCOIN_URL + "/v1/open/tick";
    String COIN_INFO_URL = KUCOIN_URL + "/v1/market/open/coins";

    String ORDER_BOOK = KUCOIN_URL + "/v1/open/orders?symbol={symbol}";
    String ACTIVE_ORDERS = KUCOIN_URL + "/order/active";
    String DEPOSIT_API = KUCOIN_URL + "/v1/account/{coin}/wallet/address";
}
