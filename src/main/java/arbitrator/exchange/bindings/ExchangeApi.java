package arbitrator.exchange.bindings;

import arbitrator.exchange.model.ApiInstrument;
import arbitrator.shared.model.KLineData;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.exchange.model.ApiOrderInfo;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.CoinInfo;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.Side;
import javafx.util.Pair;

import java.util.List;

public interface ExchangeApi {
    Exchange getExchange();
    List<ApiInstrument> getInstruments();
    List<CoinInfo> getCoinInfos();
    List<KLineData> getKLineData(final String symbol, final int interval, final int limit);

    /**
     * Usage : check if order is finished executing
     */
    List<ApiOrderInfo> getMyActiveOrders(final String symbol);
    List<CoinAndCount> getAssets();
    String getDepositAddress(final String coin);
    void sendCoinToAddress(final String coin, final String walletAddress, final double volume);
    /**
     * Usage : get accurate price
     *         is order on top of the order book
     * @param symbol eg KCS-BTC or ETHBTC
     */
    ApiOrderBook getOrderBook(final String symbol);
    Pair<String,Double> submitLimitOrder(final String symbol, final double volume, final double price, final Side side);



}
