package arbitrator;

public class Constants {
    public static final int KLINE_DATA_INTERVAL_MINUTES = 30;
    public static final int KLINE_DATA_LIMIT = 50;
    public static final int KLINE_VOLUME_MULTIPLIER = 10;
    public static final int KLINE_VOLUME_CONFIDENCE_WEIGHT = 50;
    public static final int PRICE_TREND_CONFIDENCE_WEIGHT = 30;
    public static final int ASKING_PRICE_IN_PATTERN_CONFIDENCE_WEIGHT = 20;
    public static final int ASKING_PRICE_IN_PATTERN_TOLERANCE_PERCENT = 20;


    public static final String KEYSTORE_FILE_PATH = "/media/next/UUI/ks.sh";

    public static double COIN_TRANSFER_DISCREPANCY_TOLERANCE_PC = 1;

    public static double TRADER_POLL_STEP_MILLIS = 100;
}
