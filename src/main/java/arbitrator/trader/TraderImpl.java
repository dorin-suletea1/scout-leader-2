package arbitrator.trader;

import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Trade;
import arbitrator.shared.model.TradeChain;
import arbitrator.trader.model.TraderExchangeTransaction;
import arbitrator.trader.model.TraderExitStatus;
import arbitrator.trader.model.TraderTransaction;
import arbitrator.trader.model.TraderTransferTransaction;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class TraderImpl implements Trader {
    private final ExchangeManagerProvider exchangeManagerProvider;
    private final BlockingQueue latch;

    private double minAcceptedOutput;
    private CoinAndCount initialDeposit;
    private CoinAndCount currentHoldings;

    private LinkedList<TraderTransaction> remainingTransactions;
    private volatile TraderTransaction currentTransaction;

    @Inject
    public TraderImpl(final ExchangeManagerProvider exchangeManagerProvider) {
        this.exchangeManagerProvider = exchangeManagerProvider;
        this.latch = new ArrayBlockingQueue(1);
    }

    @Override
    public boolean withState(final CoinAndCount minAcceptedOutput, final TradeChain inputChain, final CoinAndCount deposit) {
        if (isSetupValid(deposit, inputChain, minAcceptedOutput)) {
            this.minAcceptedOutput = minAcceptedOutput.getCoinCount();
            this.initialDeposit = deposit;
            this.currentHoldings = deposit;
            this.remainingTransactions = asTraderTransactions(inputChain);
            this.currentTransaction = null;
            return true;
        }
        return false;
    }

    public TraderExitStatus.ExitStatus getProfitabilityStatus() {
        try {
            if (simulateFullExecutionOutput().getCoinCount() < minAcceptedOutput) {
                return TraderExitStatus.ExitStatus.NOK_GAINS_BELLOW_THRESHOLD;
            }
        } catch (NotEnoughVolumeException e) {
            return TraderExitStatus.ExitStatus.NOK_NOT_ENOUGH_VOLUME;
        }
        return TraderExitStatus.ExitStatus.OK;
    }

    @Override
    public TraderExitStatus run() {
        TraderExitStatus.ExitStatus preRunProfitabilityStatus = getProfitabilityStatus();
        if (preRunProfitabilityStatus != TraderExitStatus.ExitStatus.OK) {
            return TraderExitStatus.abort(preRunProfitabilityStatus, currentHoldings, remainingTransactions.peekFirst().getStartsInExchange());
        }

        try {
            while ((currentTransaction = remainingTransactions.poll()) != null) {
                TraderExitStatus.ExitStatus duringRunProfitabilityStatus = getProfitabilityStatus();
                if (preRunProfitabilityStatus != TraderExitStatus.ExitStatus.OK) {
                    return TraderExitStatus.abort(duringRunProfitabilityStatus, currentHoldings, currentTransaction.getStartsInExchange());
                }

                currentTransaction.withInput(currentHoldings);
                currentTransaction.execute();
                while (latch.poll(100, TimeUnit.MILLISECONDS) == null) {
                    Optional<CoinAndCount> tradeResult = currentTransaction.getResult();
                    if (tradeResult.isPresent()) {
                        currentHoldings = tradeResult.get();
                        latch.add(1);
                    }
                }
            }
            return TraderExitStatus.success(currentHoldings, currentTransaction.getEndsInExchange());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /*** Simulates trader output from current state, if i am in the middle of the chain execution
     * and the market changes I need to exit and figure out the next steps in the caller*/
    private CoinAndCount simulateFullExecutionOutput() throws NotEnoughVolumeException {
        CoinAndCount inputForNextTrade = currentHoldings;
        for (TraderTransaction remainingTransaction : remainingTransactions) {
            remainingTransaction.withInput(inputForNextTrade);
            inputForNextTrade = remainingTransaction.precalculateExactOutput();
        }
        return inputForNextTrade;
    }

    private boolean isSetupValid(final CoinAndCount deposit, final TradeChain inputChain,
                                 final CoinAndCount minAcceptedOutput) {
        boolean chainEndsWithBaseCoin = deposit.getCoin().equals(inputChain.getEndsWithCoin());
        boolean chainStartsWithBaseCoin = deposit.getCoin().equals(inputChain.getStartsWithCoin());
        boolean minAcceptedOutputIsInBaseCurrency = minAcceptedOutput.getCoin().equals(inputChain.getEndsWithCoin());

        return chainEndsWithBaseCoin && chainStartsWithBaseCoin && minAcceptedOutputIsInBaseCurrency;
    }

    private LinkedList<TraderTransaction> asTraderTransactions(final TradeChain tradeChain) {
        LinkedList<TraderTransaction> ret = new LinkedList<>();
        for (Trade trade : tradeChain.getTrades()) {
            switch (trade.getTradeType()) {
                case TRANSFER:
                    ret.add(new TraderTransferTransaction(
                            exchangeManagerProvider.getManager(trade.getStartsWithExchange()),
                            exchangeManagerProvider.getManager(trade.getEndsWithExchange()),
                            trade.getStartsWithCoin()));
                    break;
                case EXCHANGE:
                    ret.add(new TraderExchangeTransaction(
                            exchangeManagerProvider.getManager(trade.getStartsWithExchange()),
                            trade.getInstrument().get().getInstrument()));
                    break;
                default:
                    throw new RuntimeException("Chain contains unknown trade type");
            }
        }
        return ret;
    }
}
