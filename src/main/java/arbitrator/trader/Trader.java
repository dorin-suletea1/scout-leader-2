package arbitrator.trader;

import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.TradeChain;
import arbitrator.trader.model.TraderExitStatus;

public interface Trader {
    TraderExitStatus run();
    boolean withState(final CoinAndCount minAcceptedOutput, final TradeChain inputChain, final CoinAndCount deposit);
}
