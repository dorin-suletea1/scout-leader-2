package arbitrator.trader.model;

import arbitrator.Constants;
import arbitrator.MathHelper;
import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;

import java.util.Optional;

public class TraderTransferTransaction implements TraderTransaction {
    private final ExchangeManager sourceExchangeManager;
    private final ExchangeManager destExchangeManager;

    private final String coin;
    private final CoinAndCount preExecutionHoldings;
    private final double transferFee;

    private CoinAndCount input;


    public TraderTransferTransaction(final ExchangeManager sourceExchangeManager,
                                     final ExchangeManager destExchangeManager,
                                     final String coin) {
        this.sourceExchangeManager = sourceExchangeManager;
        this.destExchangeManager = destExchangeManager;
        this.preExecutionHoldings = new CoinAndCount(coin,0d);//destExchangeManager.getBalanceForCoin(coin);
        this.transferFee = sourceExchangeManager.getCoinInfo().get(coin).getWithdrawalFee();
        this.coin = coin;
    }

    @Override
    public void withInput(final CoinAndCount input) {
        if (!input.getCoin().equals(this.coin)) {
            throw new RuntimeException("Setup performed for another input, got " + input.getCoin() + " was expecting " + coin);
        }
        this.input = input;
    }

    @Override
    public CoinAndCount precalculateExactOutput() {
        checkInitialized();
        return new CoinAndCount(input.getCoin(), input.getCoinCount() - transferFee);
    }

    @Override
    public void execute() {
        checkInitialized();
        final String destinationAddress = destExchangeManager.getDepositAddress(coin);
        sourceExchangeManager.sendCoinToAddress(input, destinationAddress);
    }

    @Override
    public Optional<CoinAndCount> getResult() {
        checkInitialized();
        final double transferAbsTolerance = MathHelper.percentOf(Constants.COIN_TRANSFER_DISCREPANCY_TOLERANCE_PC, input.getCoinCount());

        CoinAndCount destinationCoinCount = destExchangeManager.getBalanceForCoin(coin);
        if (destinationCoinCount.getCoinCount() - preExecutionHoldings.getCoinCount() > input.getCoinCount() - transferAbsTolerance) {
            return Optional.of(destinationCoinCount);
        }

        return Optional.empty();
    }

    @Override
    public Exchange getStartsInExchange() {
        return sourceExchangeManager.getExchange();
    }

    @Override
    public Exchange getEndsInExchange() {
        return destExchangeManager.getExchange();
    }

    private void checkInitialized() {
        if (input == null) {
            throw new RuntimeException("Input not initialized, you forgot to call this.withInput()");
        }
    }
}


