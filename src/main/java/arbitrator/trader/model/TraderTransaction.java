package arbitrator.trader.model;

import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;

import java.util.Optional;

public interface TraderTransaction {
    void withInput(final CoinAndCount input);
    CoinAndCount precalculateExactOutput() throws NotEnoughVolumeException;
    void execute();
    Optional<CoinAndCount> getResult();
    Exchange getStartsInExchange();
    Exchange getEndsInExchange();
}
