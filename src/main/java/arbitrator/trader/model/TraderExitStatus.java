package arbitrator.trader.model;

import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;

public class TraderExitStatus {
    private final ExitStatus status;
    private final CoinAndCount holdings;
    private final Exchange exchange;

    private TraderExitStatus(final ExitStatus status, final CoinAndCount holdings, final Exchange exchange) {
        this.status = status;
        this.holdings = holdings;
        this.exchange = exchange;
    }


    public static TraderExitStatus abort(final ExitStatus exitStatus, final CoinAndCount coinAndCount, final Exchange exchange) {
        return new TraderExitStatus(exitStatus, coinAndCount, exchange);
    }

    public static TraderExitStatus success(final CoinAndCount coinAndCount, final Exchange exchange) {
        return new TraderExitStatus(ExitStatus.OK, coinAndCount, exchange);
    }

    public ExitStatus getStatus() {
        return status;
    }

    public CoinAndCount getHoldings() {
        return holdings;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public enum ExitStatus {
        OK,
        NOK_GAINS_BELLOW_THRESHOLD,
        NOK_NOT_ENOUGH_VOLUME
    }
}
