package arbitrator.trader.model;

import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.Instrument;
import javafx.util.Pair;

import java.util.Optional;

public class TraderExchangeTransaction implements TraderTransaction {
    private final ExchangeManager exchangeManager;
    private final Instrument instrument;

    private CoinAndCount input;
    private String orderId;
    private double originalOrderQuantity;

    public TraderExchangeTransaction(final ExchangeManager currentExchange, final Instrument instrument) {
        this.exchangeManager = currentExchange;
        this.instrument = instrument;
    }

    @Override
    public void withInput(final CoinAndCount input) {
        this.input = input;
    }

    @Override
    public CoinAndCount precalculateExactOutput() throws NotEnoughVolumeException {
        checkInitialized();
        return exchangeManager.getOrderExactOutput(instrument, input);
    }

    @Override
    public void execute() {
        checkInitialized();
        Pair<String, Double> orderInfo = exchangeManager.runOrder(instrument, input);
        this.orderId = orderInfo.getKey();
        this.originalOrderQuantity = orderInfo.getValue();
    }

    @Override
    public Optional<CoinAndCount> getResult() {
        checkInitialized();
        final boolean orderStarted = orderId != null;
        if (orderStarted && !exchangeManager.hasActiveOrder(instrument, orderId)) {
            return Optional.of(new CoinAndCount(instrument.getRightCoin(), originalOrderQuantity));
        }
        return Optional.empty();
    }

    @Override
    public Exchange getStartsInExchange() {
        return exchangeManager.getExchange();
    }

    @Override
    public Exchange getEndsInExchange() {
        return exchangeManager.getExchange();
    }

    private void checkInitialized() {
        if (input == null) {
            throw new RuntimeException("Input not initialized, you forgot to call this.withInput()");
        }
    }
}
