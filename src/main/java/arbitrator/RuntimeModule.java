package arbitrator;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import arbitrator.chainer.ChainerModule;
import arbitrator.exchange.ExchangeApiModule;
import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.ExchangeManagerProviderImpl;

public class RuntimeModule extends AbstractModule {
    private static final Injector injector = Guice.createInjector(new RuntimeModule());

    public static <T> T getInjectedObject(final Class<T> klass) {
        return injector.getInstance(klass);
    }

    @Override
    protected void configure() {
        install(new ExchangeApiModule());
        install(new ChainerModule());

        bind(ExchangeManagerProvider.class).to(ExchangeManagerProviderImpl.class);
    }
}
