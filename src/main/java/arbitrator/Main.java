package arbitrator;

import arbitrator.chainer.ChainExecutionSimulator;
import arbitrator.chainer.ChainExecutionSimulatorImpl;
import arbitrator.chainer.TradeChainGenerator;
import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.ExchangeManagerProviderImpl;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.TradeChain;
import arbitrator.trader.Trader;
import arbitrator.trader.TraderImpl;

public class Main {


    public static void main(String args[]) throws NotEnoughVolumeException {
        //TODO : refresh caches every time the trade generator is executed
        Exchange currentExchange = Exchange.KUCOIN;
        CoinAndCount currentHoldings = new CoinAndCount("ETH", 0.2d);

        final CoinAndCount minimumAcceptedOutput = new CoinAndCount("ETH", currentHoldings.getCoinCount() + MathHelper.percentOf(5, currentHoldings.getCoinCount()));
        final TradeChainGenerator tradeChainGenerator = RuntimeModule.getInjectedObject(TradeChainGenerator.class);
        final ChainExecutionSimulator chainExecutionSimulator = RuntimeModule.getInjectedObject(ChainExecutionSimulatorImpl.class);
        TradeChain bestChain = tradeChainGenerator.getBestTradeChain(currentHoldings, currentExchange);
        System.out.print("Found chain " + bestChain);
        System.out.print("With output == " + chainExecutionSimulator.getEstimatedOutput(bestChain, currentHoldings));
        System.out.print("With exact == " + chainExecutionSimulator.getExactOutput(bestChain, currentHoldings));

        final ExchangeManagerProvider managerProvider = RuntimeModule.getInjectedObject(ExchangeManagerProviderImpl.class);
        Trader trader = new TraderImpl(managerProvider);
        trader.withState(minimumAcceptedOutput, bestChain, currentHoldings);
        trader.run();
    }
}
