package arbitrator;

public interface Logger {
    void log(final Exception e);
    void log(final String str);
}
