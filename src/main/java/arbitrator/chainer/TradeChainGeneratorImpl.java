package arbitrator.chainer;

import arbitrator.chainer.model.ExchangeTrade;
import arbitrator.chainer.model.TradeChainDo;
import arbitrator.chainer.model.TransferTrade;
import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.*;
import javafx.util.Pair;

import javax.inject.Inject;
import java.util.*;

public class TradeChainGeneratorImpl implements TradeChainGenerator {
    private static final int PRE_FILTER_CHAIN_COUNT = 10;
    private final ExchangeManagerProvider exchangeManagerProvider;
    private final ChainExecutionSimulator chainExecutionSimulator;
    private final CoinBlacklist coinBlackList;

    @Inject
    public TradeChainGeneratorImpl(final ExchangeManagerProvider exchangeManagerProvider, final ChainExecutionSimulator chainChainExecutionSimulator, final CoinBlacklist coinBlackList) {
        this.exchangeManagerProvider = exchangeManagerProvider;
        this.chainExecutionSimulator = chainChainExecutionSimulator;
        this.coinBlackList = coinBlackList;
    }

    @Override
    public TradeChain getBestTradeChain(final CoinAndCount baseCoinInput, final Exchange baseExchange) {
        List<Pair<InstrumentAndPrice, InstrumentAndPrice>> possibleTargets = getCompatibleInstrumentsBetweenAllExchanges();

        List<TradeChain> allChains = new ArrayList<>();
        for (Pair<InstrumentAndPrice, InstrumentAndPrice> target : possibleTargets) {
            allChains.addAll(getChainsForTarget(baseCoinInput, baseExchange, target));
        }


        allChains.sort(TradeChain.getEstimatedOutputAscendingComparator(baseCoinInput, chainExecutionSimulator));
        final List<TradeChain> preFilteredChains = allChains.subList(allChains.size() - PRE_FILTER_CHAIN_COUNT, allChains.size());

        TradeChain bestChain = null;
        double bestChainReturnCoinCount = 0;

        for (TradeChain chain : preFilteredChains) {
            try {
                double chainReturnCoinCount = chainExecutionSimulator.getExactOutput(chain, baseCoinInput).getCoinCount();
                if (chainReturnCoinCount > bestChainReturnCoinCount) {
                    bestChainReturnCoinCount = chainReturnCoinCount;
                    bestChain = chain;
                }

                System.out.println("Chain " + chain);
                System.out.println("With output     == " + chainExecutionSimulator.getEstimatedOutput(bestChain, baseCoinInput));
                System.out.println("With exact      == " + chainExecutionSimulator.getExactOutput(bestChain, baseCoinInput));
                System.out.println("With confidence == " + chainExecutionSimulator.getTradeChainExecutionConfidence(chain, baseCoinInput));

            } catch (NotEnoughVolumeException e) {
            }
        }

        return bestChain;
    }


    private List<TradeChain> getChainsForTarget(final CoinAndCount baseCoinInput, final Exchange baseExchange, final Pair<InstrumentAndPrice, InstrumentAndPrice> target) {
        boolean endsInBaseCoin = target.getValue().getInstrument().getRightCoin().equals(baseCoinInput.getCoin());

        final Optional<Trade> chainSuffix = getChainSuffix(baseCoinInput.getCoin(), target);
        if (!endsInBaseCoin && !chainSuffix.isPresent()) {
            return Collections.EMPTY_LIST;
        }
        final Optional<List<Trade>> executionTrades = getChainBody(target);
        if (!executionTrades.isPresent()) {
            return Collections.EMPTY_LIST;
        }

        List<TradeChain> possibleChains = new ArrayList<>();
        for (List<Trade> potentialPrefix : getChainPrefix(baseCoinInput, baseExchange, target)) {
            List<Trade> tradesForChain = new ArrayList<>();
            tradesForChain.addAll(potentialPrefix);
            tradesForChain.addAll(executionTrades.get());
            if (!endsInBaseCoin) {
                tradesForChain.add(chainSuffix.get());
            }
            possibleChains.add(new TradeChainDo(tradesForChain));
        }
        return possibleChains;
    }

    private Optional<Trade> getChainSuffix(final String baseCoin, final Pair<InstrumentAndPrice, InstrumentAndPrice> target) {
        return exchangeCoins(
                target.getValue().getInstrument().getExchange(),
                target.getValue().getInstrument().getRightCoin(),
                baseCoin);
    }

    private Optional<List<Trade>> getChainBody(final Pair<InstrumentAndPrice, InstrumentAndPrice> target) {
        boolean sameExchange = target.getKey().getInstrument().getExchange() == target.getValue().getInstrument().getExchange();

        if (sameExchange) {
            Optional<Trade> legOne = exchangeCoins(
                    target.getKey().getInstrument().getExchange(),
                    target.getKey().getInstrument().getLeftCoin(),
                    target.getKey().getInstrument().getRightCoin());
            if (!legOne.isPresent()) {
                return Optional.empty();
            }
            Optional<Trade> legTwo = exchangeCoins(
                    target.getKey().getInstrument().getExchange(),
                    legOne.get().getEndsWithCoin(),
                    target.getValue().getInstrument().getLeftCoin());
            if (!legTwo.isPresent()) {
                return Optional.empty();
            }
            return Optional.of(Arrays.asList(new Trade[]{legOne.get(), legTwo.get()}));
        }

        Optional<Trade> legOne = exchangeCoins(
                target.getKey().getInstrument().getExchange(),
                target.getKey().getInstrument().getLeftCoin(),
                target.getKey().getInstrument().getRightCoin());
        if (!legOne.isPresent()) {
            return Optional.empty();
        }

        Optional<Trade> transferLeg = transferCoinSimple(
                legOne.get().getEndsWithCoin(),
                legOne.get().getEndsWithExchange(),
                target.getValue().getInstrument().getExchange());
        if (!transferLeg.isPresent()) {
            return Optional.empty();
        }

        Optional<Trade> legTwo = exchangeCoins(
                transferLeg.get().getEndsWithExchange(),
                transferLeg.get().getEndsWithCoin(),
                target.getValue().getInstrument().getRightCoin());
        if (!legTwo.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(Arrays.asList(new Trade[]{legOne.get(), transferLeg.get(), legTwo.get()}));
    }

    private List<List<Trade>> getChainPrefix(final CoinAndCount baseCoinInput, final Exchange baseExchange, final Pair<InstrumentAndPrice, InstrumentAndPrice> target) {
        final boolean startsInBaseCoin = target.getKey().getInstrument().getLeftCoin().equals(baseCoinInput.getCoin());
        final boolean startsInBaseExchange = target.getKey().getInstrument().getExchange() == baseExchange;

        if (!startsInBaseCoin && !startsInBaseExchange) {
            List<List<Trade>> ret = new ArrayList<>();
            Optional<List<Trade>> convertAndSend = makeConvertAndSendPrefix(baseExchange, baseCoinInput.getCoin(), target);
            Optional<List<Trade>> sendAndConvert = makeSendAndConvertPrefix(baseExchange, baseCoinInput.getCoin(), target);
            if (convertAndSend.isPresent()) {
                ret.add(convertAndSend.get());
            }
            if (sendAndConvert.isPresent()) {
                ret.add(sendAndConvert.get());
            }
            return ret;
        }

        if (!startsInBaseCoin && startsInBaseExchange) {
            Optional<Trade> exchangeTrade = exchangeCoins(baseExchange, baseCoinInput.getCoin(), target.getKey().getInstrument().getLeftCoin());
            if (exchangeTrade.isPresent()) {
                return Collections.singletonList(Collections.singletonList(exchangeTrade.get()));
            }
            return Collections.EMPTY_LIST;
        }

        if (startsInBaseCoin && !startsInBaseExchange) {
            Optional<Trade> transfer = transferCoinSimple(baseCoinInput.getCoin(), baseExchange, target.getKey().getInstrument().getExchange());
            if (transfer.isPresent()) {
                return Collections.singletonList(Collections.singletonList(transfer.get()));
            }
            return Collections.EMPTY_LIST;
        }

        if (startsInBaseCoin && startsInBaseExchange) {
            return Collections.singletonList(Collections.EMPTY_LIST);
        }

        throw new RuntimeException("Can not find valid chain prefix for trade " + target);
    }


    private List<Pair<InstrumentAndPrice, InstrumentAndPrice>> getCompatibleInstrumentsBetweenAllExchanges() {
        List<Pair<InstrumentAndPrice, InstrumentAndPrice>> ret = new ArrayList<>();
        Set<Exchange> supportedExchanges = exchangeManagerProvider.getSupportedExchanges();
        for (Exchange fromExchange : supportedExchanges) {
            for (Exchange toExchange : supportedExchanges) {
                if (fromExchange != toExchange) {
                    ret.addAll(getPossibleCrossExchangeTrades(
                            exchangeManagerProvider.getManager(fromExchange).getInstruments(),
                            exchangeManagerProvider.getManager(toExchange).getInstruments()
                    ));
                }
            }
        }
        return ret;
    }

    private List<Pair<InstrumentAndPrice, InstrumentAndPrice>> getPossibleCrossExchangeTrades(List<InstrumentAndPrice> fromExchange, List<InstrumentAndPrice> toExchange) {
        List<Pair<InstrumentAndPrice, InstrumentAndPrice>> ret = new ArrayList<>();

        for (InstrumentAndPrice a : fromExchange) {
            for (InstrumentAndPrice b : toExchange) {
                final boolean outTransferOk = a.getInstrument().getRightCoin().equals(b.getInstrument().getLeftCoin());
                final boolean inTransferOk = b.getInstrument().getRightCoin().equals(a.getInstrument().getLeftCoin());
                if (outTransferOk && inTransferOk) {
                    ret.add(new Pair(a, b));
                    ret.add(new Pair(b, a));
                }
            }
        }
        return ret;
    }

    /**
     * @return Many chains for 1 exchangeTrade -> all transferOptions
     **/
    private Optional<List<Trade>> makeConvertAndSendPrefix(final Exchange baseExchange,
                                                           final String baseCoin,
                                                           final Pair<InstrumentAndPrice, InstrumentAndPrice> target) {
        Optional<Trade> exchange = exchangeCoins(baseExchange, baseCoin, target.getKey().getInstrument().getLeftCoin());
        if (exchange.isPresent()) {
            Optional<Trade> transfer = transferCoinSimple(exchange.get().getEndsWithCoin(), exchange.get().getEndsWithExchange(), target.getKey().getInstrument().getExchange());
            if (transfer.isPresent()) {
                return Optional.of(Arrays.asList(new Trade[]{exchange.get(), transfer.get()}));
            }
        }
        return Optional.empty();
    }

    /**
     * @return all transferOptions -> 1 exchange for each
     **/
    private Optional<List<Trade>> makeSendAndConvertPrefix(final Exchange baseExchange,
                                                           final String baseCoin,
                                                           final Pair<InstrumentAndPrice, InstrumentAndPrice> target) {
        Optional<Trade> transfer = transferCoinSimple(baseCoin, baseExchange, target.getKey().getInstrument().getExchange());
        if (transfer.isPresent()) {
            Optional<Trade> exchange = exchangeCoins(transfer.get().getEndsWithExchange(), transfer.get().getEndsWithCoin(), target.getKey().getInstrument().getLeftCoin());
            if (exchange.isPresent()) {
                return Optional.of(Arrays.asList(new Trade[]{transfer.get(), exchange.get()}));
            }
        }
        return Optional.empty();
    }

    private Optional<Trade> exchangeCoins(final Exchange exchange, final String iHaveCoin, final String iWantCoin) {
        final ExchangeManager manager = exchangeManagerProvider.getManager(exchange);
        for (InstrumentAndPrice instrument : manager.getInstruments()) {
            if (instrument.getInstrument().getLeftCoin().equals(iHaveCoin) && instrument.getInstrument().getRightCoin().equals(iWantCoin)) {
                return Optional.of(new ExchangeTrade(instrument));
            }
        }
        return Optional.empty();
    }

    private Optional<Trade> transferCoinSimple(final String coin, final Exchange fromExchange, final Exchange toExchange) {
        if (isCoinTransferable(coin, fromExchange, toExchange, this.coinBlackList)) {
            final double transferFees = exchangeManagerProvider.getManager(fromExchange).getCoinInfo().get(coin).getWithdrawalFee();
            Trade transferTrade = new TransferTrade(coin, transferFees, fromExchange, toExchange);
            return Optional.of(transferTrade);
        }
        return Optional.empty();
    }

    private boolean isCoinTransferable(final String coin, final Exchange fromExchange, final Exchange toExchange, final CoinBlacklist coinBlacklist) {
        final ExchangeManager fromExchangeManager = exchangeManagerProvider.getManager(fromExchange);
        final ExchangeManager toExchangeManager = exchangeManagerProvider.getManager(toExchange);

        boolean coinBlacklisted = coinBlacklist.isCoinBlackListed(fromExchange, coin) || coinBlacklist.isCoinBlackListed(toExchange, coin);
        boolean coinSupported = fromExchangeManager.getCoinInfo().containsKey(coin) && toExchangeManager.getCoinInfo().containsKey(coin);
        boolean canTransfer = !coinBlacklisted && coinSupported &&
                exchangeManagerProvider.getManager(fromExchange).getCoinInfo().get(coin).isActive() &&
                exchangeManagerProvider.getManager(toExchange).getCoinInfo().get(coin).isActive();
        return canTransfer;
    }
}
