package arbitrator.chainer;

import arbitrator.Constants;
import arbitrator.MathHelper;
import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.*;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ChainExecutionSimulatorImpl implements ChainExecutionSimulator {
    private final ExchangeManagerProvider exchangeManagerProvider;

    @Inject
    public ChainExecutionSimulatorImpl(final ExchangeManagerProvider exchangeManagerProvider) {
        this.exchangeManagerProvider = exchangeManagerProvider;
    }

    @Override
    public CoinAndCount getEstimatedOutput(final TradeChain chain, final CoinAndCount input) {
        CoinAndCount inputForNextTrade = input;
        CoinAndCount coinAndCount = null;
        for (Trade trade : chain.getTrades()) {
            coinAndCount = trade.getEstimatedOutput(inputForNextTrade);
            inputForNextTrade = coinAndCount;
        }

        return coinAndCount;
    }

    @Override
    public CoinAndCount getExactOutput(final TradeChain chain, final CoinAndCount input) throws NotEnoughVolumeException {
        CoinAndCount inputForNextTrade = input;
        for (Trade trade : chain.getTrades()) {
            inputForNextTrade = trade.getExactOutput(inputForNextTrade, exchangeManagerProvider);
        }
        return inputForNextTrade;
    }

    @Override
    public double getTradeChainExecutionConfidence(final TradeChain chain, final CoinAndCount input)  {
        //
        // only exchange trades are considered since they are not instant or guaranteed
        //
        int totalConfidenceFactor = 0;
        int numberOfExchangeTrades = 0;
        CoinAndCount inputForNextTrade = input;
        for (Trade trade : chain.getTrades()) {
            if (trade.getTradeType() == Trade.Type.EXCHANGE) {
                totalConfidenceFactor += getTradeChainExecutionConfidence(
                        trade,
                        inputForNextTrade,
                        Constants.KLINE_DATA_INTERVAL_MINUTES,
                        Constants.KLINE_DATA_LIMIT,
                        Constants.KLINE_VOLUME_CONFIDENCE_WEIGHT,
                        Constants.KLINE_VOLUME_MULTIPLIER,
                        Constants.PRICE_TREND_CONFIDENCE_WEIGHT,
                        Constants.ASKING_PRICE_IN_PATTERN_CONFIDENCE_WEIGHT,
                        Constants.ASKING_PRICE_IN_PATTERN_TOLERANCE_PERCENT);
                numberOfExchangeTrades++;
            }
            inputForNextTrade = trade.getEstimatedOutput(inputForNextTrade);
        }

        return totalConfidenceFactor / numberOfExchangeTrades;
    }


    private double getTradeChainExecutionConfidence(final Trade trade,
                                                    final CoinAndCount tradeInput,
                                                    final int klineInterval,
                                                    final int klineDataLimit,
                                                    final int volumeMatchWeight,
                                                    final int volumeMultiplier,
                                                    final int priceTrendingUpWeight,
                                                    final int askPriceFitsPatternWeight,
                                                    final int askPriceTolerancePercent) {

        ExchangeManager exchangeManager = this.exchangeManagerProvider.getManager(trade.getStartsWithExchange());
        List<KLineData> kLineData = exchangeManager.getChronologicallySortedKLineData(trade.getInstrument().get().getInstrument(), klineInterval, klineDataLimit);
        final double executionVolume = trade.getInstrument().get().getInstrument().getSide() == Side.BUY ? tradeInput.getCoinCount() * trade.getInstrument().get().getPrice() : tradeInput.getCoinCount();

        final double volumeMathFactor = volumeMatchWeight * getVolumeCanBeFilledConfidence(kLineData, executionVolume * volumeMultiplier);
        final double priceTrendingUpFactor = priceTrendingUpWeight * getPvt(kLineData);
        final double askPriceFitsFactor = askPriceFitsPatternWeight * (MathHelper.percentGain(trade.getInstrument().get().getPrice(), getHistoricalAveragePrice(kLineData)) < askPriceTolerancePercent ? 1 : 0);
        final double confidenceFactor = volumeMathFactor + priceTrendingUpFactor + askPriceFitsFactor;

        return confidenceFactor;
    }

    private double getVolumeCanBeFilledConfidence(List<KLineData> kLineData, double executionVolume) {
        List<Double> volumeFitList = kLineData.stream().map(kline -> kline.getVolume() >= executionVolume ? 1d : 0d).collect(Collectors.toList());
        return Math.round(MathHelper.calculateWeightedAverage(volumeFitList));
    }

    private double getHistoricalAveragePrice(List<KLineData> kLineData) {
        return MathHelper.calculateWeightedAverage(kLineData.stream().map(kline -> kline.getCandleClosePrice()).collect(Collectors.toList()));
    }

    /**
     * The higher the pvt, the more likely price to go down
     */
    private double getPvt(List<KLineData> kLineData) {
        LinkedList<Double> pvts = new LinkedList<>();
        pvts.add(0.0d);
        for (int i = 1; i < kLineData.size(); i++) {
            double currentClose = kLineData.get(i).getCandleClosePrice();
            double previousClose = kLineData.get(i - 1).getCandleClosePrice();
            double currentVolume = kLineData.get(i).getVolume();
            Double newPvt = (((currentClose - previousClose) / previousClose) * currentVolume) + pvts.peek();
            pvts.add(newPvt);
        }
        final Long averageWeightedPvt = Math.round(MathHelper.calculateWeightedAverage(pvts));
        return averageWeightedPvt > 0 ? 1 / averageWeightedPvt : 0;
    }
}
