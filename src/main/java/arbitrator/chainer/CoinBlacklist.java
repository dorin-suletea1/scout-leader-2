package arbitrator.chainer;

import arbitrator.shared.model.Exchange;

public interface CoinBlacklist {
    void blackListCoins(final Exchange exchange, final String coin);
    boolean isCoinBlackListed(final Exchange exchange, final String coin);
}
