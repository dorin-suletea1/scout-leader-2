package arbitrator.chainer.model;

import arbitrator.StringHelper;
import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.*;

import java.util.Optional;

public class ExchangeTrade implements Trade {
    private final InstrumentAndPrice instrument;

    public ExchangeTrade(final InstrumentAndPrice instrument) {
        this.instrument = instrument;
    }

    @Override
    public String getStartsWithCoin() {
        return instrument.getInstrument().getLeftCoin();
    }

    @Override
    public String getEndsWithCoin() {
        return instrument.getInstrument().getRightCoin();
    }

    @Override
    public Exchange getStartsWithExchange() {
        return instrument.getInstrument().getExchange();
    }

    @Override
    public Exchange getEndsWithExchange() {
        return instrument.getInstrument().getExchange();
    }

    @Override
    public CoinAndCount getEstimatedOutput(final CoinAndCount fromInput) {
        final String resultCoin = instrument.getInstrument().getRightCoin();
        final double resultCoinCount = instrument.getPrice() * fromInput.getCoinCount();
        return new CoinAndCount(resultCoin, resultCoinCount);
    }

    @Override
    public CoinAndCount getExactOutput(final CoinAndCount fromInput, final ExchangeManagerProvider exchangeManagerProvider) throws NotEnoughVolumeException {
        final ExchangeManager manager = exchangeManagerProvider.getManager(instrument.getInstrument().getExchange());
        return manager.getOrderExactOutput(instrument.getInstrument(), fromInput);
    }


    @Override
    public Type getTradeType() {
        return Type.EXCHANGE;
    }

    @Override
    public Optional<InstrumentAndPrice> getInstrument() {
        return Optional.of(instrument);
    }

    @Override
    public String toString() {
        return "Exchange : " + instrument.getInstrument().getLeftCoin() + " --- " + instrument.getInstrument().getRightCoin() + " " + instrument.getInstrument().getExchange()
                + " [Price " + StringHelper.formattedDouble(instrument.getPrice()) + "]"
                + " [RevPrice " + StringHelper.formattedDouble(1 / instrument.getPrice()) + "]";
    }
}
