package arbitrator.chainer.model;

import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.model.*;

import java.util.Optional;

public class TransferTrade implements Trade {
    private final String coinSymbol;
    private final double withdrawalFee;
    private final Exchange fromExchange;
    private final Exchange toExchange;

    public TransferTrade(final String coinSymbol, final double withdrawalFee, final Exchange fromExchange, final Exchange toExchange) {
        this.coinSymbol = coinSymbol;
        this.withdrawalFee = withdrawalFee;
        this.fromExchange = fromExchange;
        this.toExchange = toExchange;
    }

    @Override
    public String getStartsWithCoin() {
        return coinSymbol;
    }

    @Override
    public String getEndsWithCoin() {
        return coinSymbol;
    }

    @Override
    public Exchange getStartsWithExchange() {
        return fromExchange;
    }

    @Override
    public Exchange getEndsWithExchange() {
        return toExchange;
    }

    @Override
    public CoinAndCount getEstimatedOutput(final CoinAndCount fromInput) {
        return new CoinAndCount(coinSymbol, fromInput.getCoinCount() - withdrawalFee);
    }

    @Override
    public CoinAndCount getExactOutput(final CoinAndCount fromInput, final ExchangeManagerProvider exchangeManagerProvider) {
        return new CoinAndCount(fromInput.getCoin(), fromInput.getCoinCount() - withdrawalFee);
    }

    @Override
    public Type getTradeType() {
        return Type.TRANSFER;
    }

    @Override
    public Optional<InstrumentAndPrice> getInstrument() {
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "Transfer : " + coinSymbol + " " + fromExchange + " -- " + toExchange + " ";
    }
}
