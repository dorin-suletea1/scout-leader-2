package arbitrator.chainer.model;

import arbitrator.shared.model.Trade;
import arbitrator.shared.model.TradeChain;

import java.util.Collections;
import java.util.List;

public class TradeChainDo implements TradeChain {
    private final List<Trade> trades;

    public TradeChainDo(final List<Trade> trades) {
        this.trades = trades;
    }

    @Override
    public String getStartsWithCoin() {
        return trades.get(0).getStartsWithCoin();
    }

    @Override
    public String getEndsWithCoin() {
        return trades.get(trades.size() - 1).getEndsWithCoin();
    }

    @Override
    public List<Trade> getTrades() {
        return Collections.unmodifiableList(trades);
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        for (Trade trade : trades) {
            ret.append(trade.toString() + "\n");
        }
        ret.append("------\n");
        return ret.toString();
    }
}
