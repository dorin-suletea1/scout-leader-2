package arbitrator.chainer;

import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.TradeChain;

public interface TradeChainGenerator {
    TradeChain getBestTradeChain(final CoinAndCount baseCoinInput, final Exchange baseExchange);
}
