package arbitrator.chainer;

import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.TradeChain;

public interface ChainExecutionSimulator {
    CoinAndCount getEstimatedOutput(final TradeChain chain, final CoinAndCount input);

    CoinAndCount getExactOutput(final TradeChain chain, final CoinAndCount input) throws NotEnoughVolumeException;

    /**
     * @returns 1 | 0 ; 1 = 100% will execute ; 0 = 0%   will execute
     * volume = 10 * my volume                     = 1  * weight = 50%
     * price  = is trending up                     = 1  * weight = 30% the smaller the average PVT the better(Volume Price Trend Indicator) PVT = [((CurrentClose - PreviousClose) / PreviousClose) x Volume] + PreviousPVT
     * myPrice > avgPrice + 20%  = 1 : 0  * weight = 20%
     **/
    double getTradeChainExecutionConfidence(final TradeChain chain,final CoinAndCount input);
}
