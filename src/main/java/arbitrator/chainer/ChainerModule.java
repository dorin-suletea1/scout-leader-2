package arbitrator.chainer;

import com.google.inject.AbstractModule;

public class ChainerModule extends AbstractModule{
    @Override
    protected void configure() {
        bind(CoinBlacklist.class).to(CoinBlacklistImpl.class);
        bind(TradeChainGenerator.class).to(TradeChainGeneratorImpl.class);
        bind(ChainExecutionSimulator.class).to(ChainExecutionSimulatorImpl.class);
    }
}
