package arbitrator.chainer;

import arbitrator.shared.model.Exchange;

import javax.inject.Singleton;
import java.util.*;

@Singleton
public class CoinBlacklistImpl implements CoinBlacklist {
    private final Map<Exchange, List<String>> blacklistCoins;


    public CoinBlacklistImpl() {
        blacklistCoins = new HashMap<>();
        init();
    }

    private void init() {
        this.blackListCoins(Exchange.BINANCE, "PIVX");
        this.blackListCoins(Exchange.BINANCE, "STEEM");
        this.blackListCoins(Exchange.BINANCE, "VIA");
        this.blackListCoins(Exchange.BINANCE, "POE");
        this.blackListCoins(Exchange.BINANCE, "INS");
        this.blackListCoins(Exchange.BINANCE, "IOST");
        this.blackListCoins(Exchange.BINANCE, "RPX");
        this.blackListCoins(Exchange.BINANCE, "ZIL");

    }

    public void blackListCoins(final Exchange exchange, final String coin) {
        if (!blacklistCoins.containsKey(exchange)) {
            blacklistCoins.put(exchange, new ArrayList<>(Collections.singleton(coin)));
            return;
        }

        List<String> exchangeBlacklist = blacklistCoins.get(exchange);
        exchangeBlacklist.add(coin);
    }

    public boolean isCoinBlackListed(final Exchange exchange, final String coin) {
        if (!blacklistCoins.containsKey(exchange)) {
            return false;
        }
        return blacklistCoins.get(exchange).contains(coin);
    }
}
