package arbitrator.shared;

import arbitrator.exchange.BinanceManager;
import arbitrator.exchange.ExchangeManager;
import arbitrator.exchange.KucoinManager;
import arbitrator.shared.model.Exchange;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ExchangeManagerProviderImpl implements ExchangeManagerProvider {
    private Map<Exchange, ExchangeManager> exchangeManagerMap;

    @Inject
    public ExchangeManagerProviderImpl(final BinanceManager binanceManager, final KucoinManager kucoinManager) {
        exchangeManagerMap = new HashMap<>();
        exchangeManagerMap.put(Exchange.BINANCE, binanceManager);
        exchangeManagerMap.put(Exchange.KUCOIN, kucoinManager);
    }

    @Override
    public ExchangeManager getManager(final Exchange exchange) {
        return exchangeManagerMap.get(exchange);
    }

    public Set<Exchange> getSupportedExchanges() {
        return Collections.unmodifiableSet(exchangeManagerMap.keySet());
    }

}
