package arbitrator.shared.model;

import arbitrator.shared.ExchangeManagerProvider;
import arbitrator.shared.NotEnoughVolumeException;

import java.util.Optional;

public interface Trade {
    String getStartsWithCoin();
    String getEndsWithCoin();
    Exchange getStartsWithExchange();
    Exchange getEndsWithExchange();
    Type getTradeType();
    Optional<InstrumentAndPrice> getInstrument();

    CoinAndCount getEstimatedOutput(final CoinAndCount fromInput);
    CoinAndCount getExactOutput(final CoinAndCount fromInput, final ExchangeManagerProvider exchangeManagerProvider) throws NotEnoughVolumeException;

    enum Type {
        EXCHANGE,
        TRANSFER
    }
}
