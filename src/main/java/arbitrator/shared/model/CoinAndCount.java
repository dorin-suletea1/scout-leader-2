package arbitrator.shared.model;

import arbitrator.StringHelper;

public class CoinAndCount {
    private final String coin;
    private final Double coinCount;

    public CoinAndCount(final String coin, final Double coinCount) {
        this.coin = coin;
        this.coinCount = coinCount;
    }

    public String getCoin() {
        return coin;
    }

    public Double getCoinCount() {
        return coinCount;
    }

    @Override
    public String toString() {
        return "(" + StringHelper.formattedDouble(getCoinCount()) + " " + getCoin() + ")";
    }
}
