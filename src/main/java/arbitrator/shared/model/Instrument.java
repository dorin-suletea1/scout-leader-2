package arbitrator.shared.model;

/**
 * Created by next on 12/15/17.
 */
public class Instrument {
    private final String leftCoin;
    private final String rightCoin;
    private final Side side;
    private final Exchange exchange;

    public Instrument(final String leftCoin, final String rightCoin, final Side side, final Exchange exchange) {
        this.leftCoin = leftCoin;
        this.rightCoin = rightCoin;
        this.side = side;
        this.exchange = exchange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instrument instrument = (Instrument) o;

        if (!leftCoin.equals(instrument.leftCoin)) return false;
        if (!rightCoin.equals(instrument.rightCoin)) return false;
        return exchange == instrument.exchange;
    }

    @Override
    public int hashCode() {
        int result = leftCoin.hashCode();
        result = 31 * result + rightCoin.hashCode();
        result = 31 * result + exchange.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "leftCoin='" + leftCoin + '\'' +
                ", rightCoin='" + rightCoin + '\'' +
                ", exchange=" + exchange +
                ", side=" + side +
                '}';
    }

    public String getLeftCoin() {
        return leftCoin;
    }

    public String getRightCoin() {
        return rightCoin;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public Side getSide() {
        return side;
    }
}
