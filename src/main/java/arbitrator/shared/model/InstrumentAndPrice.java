package arbitrator.shared.model;

import java.util.Objects;

public class InstrumentAndPrice {
    private final Instrument instrument;
    private final double price;

    public InstrumentAndPrice(final Instrument instrument, final double price) {
        this.instrument = instrument;
        this.price = price;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final InstrumentAndPrice that = (InstrumentAndPrice) o;
        return Double.compare(that.price, price) == 0 &&
                Objects.equals(instrument, that.instrument);
    }

    @Override
    public int hashCode() {
        return Objects.hash(instrument, price);
    }

    @Override
    public String toString() {
        return "InstrumentAndPrice{" +
                "instrument=" + instrument +
                ", price=" + price +
                '}';
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public double getPrice() {
        return price;
    }
}
