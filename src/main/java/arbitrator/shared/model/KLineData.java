package arbitrator.shared.model;

public class KLineData {
    private final double volume;
    private final double highPrice;
    private final double lowPrice;
    private final long candleOpenTs;
    private final double candleClosePrice;

    public KLineData(final double volume, final double highPrice, final double lowPrice, final long candleOpenTs, final double candleClosePrice) {
        this.volume = volume;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.candleOpenTs = candleOpenTs;
        this.candleClosePrice = candleClosePrice;
    }

    public double getVolume() {
        return volume;
    }

    public double getHighPrice() {
        return highPrice;
    }

    public double getLowPrice() {
        return lowPrice;
    }

    public long getCandleOpenTs() {
        return candleOpenTs;
    }

    public double getCandleClosePrice() {
        return candleClosePrice;
    }
}
