package arbitrator.shared.model;

import arbitrator.chainer.ChainExecutionSimulator;

import java.util.Comparator;
import java.util.List;

public interface TradeChain {
    String getStartsWithCoin();

    String getEndsWithCoin();

    List<Trade> getTrades();

    static Comparator<TradeChain> getEstimatedOutputAscendingComparator(final CoinAndCount chainInput, final ChainExecutionSimulator chainExecutionSimulator) {
        return (o1, o2) -> {
            if (chainExecutionSimulator.getEstimatedOutput(o1, chainInput).getCoinCount() == chainExecutionSimulator.getEstimatedOutput(o2, chainInput).getCoinCount()) {
                return 0;
            }
            if (chainExecutionSimulator.getEstimatedOutput(o1, chainInput).getCoinCount() > chainExecutionSimulator.getEstimatedOutput(o2, chainInput).getCoinCount()) {
                return 1;
            }
            return -1;
        };
    }
}
