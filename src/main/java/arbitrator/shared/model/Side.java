package arbitrator.shared.model;

public enum Side {
    SELL,
    BUY;

    public static Side fromString(final String side){
        return Side.valueOf(side);
    }
}
