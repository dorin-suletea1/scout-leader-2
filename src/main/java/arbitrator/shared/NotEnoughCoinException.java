package arbitrator.shared;

public class NotEnoughCoinException extends RuntimeException {
    public NotEnoughCoinException(final String message) {
        super(message);
    }
}
