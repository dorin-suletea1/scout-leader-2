package arbitrator.shared;

import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.model.Exchange;

import java.util.Set;

public interface ExchangeManagerProvider {
    ExchangeManager getManager(final Exchange exchange);
    Set<Exchange> getSupportedExchanges();
}
