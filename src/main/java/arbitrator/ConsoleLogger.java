package arbitrator;

public class ConsoleLogger implements Logger{
    @Override
    public void log(final Exception e) {
        System.out.println(e);
    }

    @Override
    public void log(final String str) {
        System.out.println(str);
    }
}
