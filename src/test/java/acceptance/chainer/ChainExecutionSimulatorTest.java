package acceptance.chainer;

import acceptance.setup.AcceptanceTest;
import acceptance.setup.api.BinanceApiForTesting;
import com.google.inject.Inject;
import arbitrator.chainer.model.ExchangeTrade;
import arbitrator.shared.model.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import static com.shazam.gwen.Gwen.*;


public class ChainExecutionSimulatorTest extends AcceptanceTest {
    @Inject
    ChainExecutionSimulatorUser user;

    @Inject
    private BinanceApiForTesting exchange;

    private final String BTC = "BTC";
    private final String ETH = "ETH";
    private final double ONE = 1d;
    private final double BTC_PRICE_IN_ETH = 2d;

    private final CoinAndCount ONE_BTC = new CoinAndCount("BTC",1d);

    private final int ZERO_VOLUME = 0;
    private final int ZERO_PRICE = 0;
    private final String SYMBOL = "ETHBTC";


    @Test
    public void confidenceIsZeroIfConditionsAreUnfavorable() {
        given(exchange).hasKlines(SYMBOL, Arrays.asList(
                new KLineData(ZERO_VOLUME, ZERO_PRICE, ZERO_PRICE, TimeUnit.MINUTES.toMillis(0), ZERO_PRICE),
                new KLineData(ZERO_VOLUME, ZERO_PRICE, ZERO_PRICE, TimeUnit.MINUTES.toMillis(30), ZERO_PRICE))
        );
        given(user).hasInputCoin(ONE_BTC);
        given(user).hasTradesInChain(Collections.singletonList(
                new ExchangeTrade(new InstrumentAndPrice(new Instrument(BTC,ETH, Side.BUY,Exchange.BINANCE),BTC_PRICE_IN_ETH)))
        );
        when(user).asksForConfidenceFactor();
        then(user).receivesConfidenceFactorOf(0);
    }

    @Test
    public void confidenceCalculatedForCorrectVolume() {
        //2 trades, btc -eth , eth-ltc, make sure that volume for confidence of the second trade is the output of first trade
        // all parameters are ok, but trade 2 does not have enough volume, should reflect in the confidence
    }

    @Test
    public void confidenceIsOneIfConditionsAreFavorable() {
        // 2 klines with
        // price uptranding
        // my price is ok
        // has volume
    }

    @Test
    public void confidenceIsHalfIfOnlyVolumeIsFavorable() {
        // 2 klines with
        // price uptranding
        // my price is ok
        // has volume
    }

    @Test
    public void confidenceIsTwoThirdsIfPriceDownTrending() {
        // 2 klines with
        // price uptranding
        // my price is ok
        // has volume
    }

    @Test
    public void weightIsSignificantIfDataIsRecent() {
    }

    @Test
    public void weightIsLessSignificantIfDataIsOld() {
    }

    @Test
    public void maxConfidenceFactorIsOne() {
    }

    @Test
    public void transferTradesDoNotAffectConfidence() {
    }

    @Test
    public void confidenceIsAgnosticOFChainLength() {
    }
}
