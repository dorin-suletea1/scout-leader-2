package acceptance.chainer;

import com.shazam.gwen.collaborators.Actor;
import com.shazam.gwen.collaborators.Arranger;
import com.shazam.gwen.collaborators.Asserter;
import arbitrator.chainer.ChainExecutionSimulator;
import arbitrator.chainer.model.TradeChainDo;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Trade;
import arbitrator.shared.model.TradeChain;
import org.junit.Assert;

import javax.inject.Inject;
import java.util.List;

public class ChainExecutionSimulatorUser implements Actor, Asserter, Arranger {
    private final ChainExecutionSimulator chainExecutionSimulator;

    private CoinAndCount coinInput;
    private TradeChain chain;

    private double lastReceivedConfidence;

    @Inject
    public ChainExecutionSimulatorUser(final ChainExecutionSimulator chainExecutionSimulator) {
        this.chainExecutionSimulator = chainExecutionSimulator;
    }

    public void asksForConfidenceFactor(){
        lastReceivedConfidence = chainExecutionSimulator.getTradeChainExecutionConfidence(chain, coinInput);
    }

    public void hasTradesInChain(List<Trade> trades) {
        chain = new TradeChainDo(trades);
    }

    public void hasInputCoin(CoinAndCount coinInput) {
        this.coinInput = coinInput;
    }

    public void receivesConfidenceFactorOf(final double expectedConfidenceFactor) {
        Assert.assertEquals(lastReceivedConfidence,expectedConfidenceFactor,0.00000001);
    }
}
