package acceptance.manages;

import acceptance.setup.AcceptanceTest;
import acceptance.setup.api.BinanceApiForTesting;
import arbitrator.MathHelper;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Exchange;
import arbitrator.shared.model.Instrument;
import arbitrator.shared.model.Side;
import com.google.inject.Inject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static com.shazam.gwen.Gwen.*;

public class ExchangeManagerTest extends AcceptanceTest {
    private final static String BTC = "BTC";
    private final static String ETH = "ETH";
    private final static double ZERO = 0d;
    private final static double ONE = 1d;
    private final static double QUARTER = 0.25;
    private final static double TWO = 2d;
    private final static double THREE = 3d;
    private final static double FOUR = 4d;
    private final Instrument WITH_ETH_BUY_BTC = new Instrument(ETH, BTC, Side.BUY, Exchange.BINANCE);
    private final Instrument SELL_ETH_FOR_BTC = new Instrument(BTC, ETH, Side.SELL, Exchange.BINANCE);
    private final String ETH_BTC_SYMBOL = "BTCETH";
    private final CoinAndCount ONE_BTC = new CoinAndCount(BTC, ONE);
    private final CoinAndCount QUARTER_BTC = new CoinAndCount(BTC, QUARTER);
    private final CoinAndCount ONE_ETH = new CoinAndCount(ETH, ONE);
    private final CoinAndCount TWO_ETH = new CoinAndCount(ETH, TWO);
    private final CoinAndCount THREE_ETH = new CoinAndCount(ETH, THREE);
    @Inject
    private BinanceApiForTesting exchangeAccount;
    @Inject
    private BinanceManagerUser manager;

    @Before
    public void setup() {
        exchangeAccount.reset();
        manager.reset();
    }

    @Test
    public void getBalanceIfCoinMissingReturnsZero() {
        given(exchangeAccount).doesNotHaveCoinBalance(BTC);
        when(manager).asksForBalance(BTC);
        then(manager).validatesThatHasReceived(new CoinAndCount(BTC, ZERO));
    }

    @Test
    public void getBalanceIfCoinExistsReturnsValue() {
        given(exchangeAccount).hasCoinBalance(BTC, ONE);
        when(manager).asksForBalance(BTC);
        then(manager).validatesThatHasReceived(new CoinAndCount(BTC, ONE));
    }

    @Test
    public void sendingMoreCoinsThanOwnedThrowsException() {
        given(exchangeAccount).hasCoinBalance(BTC, ONE);
        when(manager).sendsCoin(BTC, TWO);
        then(manager).validatesThatHasReceivedNotEnoughCoinException();
    }

    @Test
    public void sendingLessCoinsThanOwnedExecutesTheTransfer() {
        given(exchangeAccount).hasCoinBalance(BTC, TWO);
        when(manager).sendsCoin(BTC, ONE);
        then(manager).validatesThatHasExactCoinCount(BTC, ONE);
    }

    /**
     * I have 3 ETH
     * First entry : for 2 of them i buy one btc since price is 2
     * Second entry : for the remaining 1 i buy 0.25 since price is 4
     */
    @Test
    public void dryRunOrderBuyAcrossTwoOrders() {
        //TODO : check caching exchange manager #89
        double firstTransactionOutcome = ONE_BTC.getCoinCount() - getCommission(TWO_ETH);
        double secondTransactionOutcome = QUARTER_BTC.getCoinCount() - getCommission(ONE_ETH.getCoinCount() - getCommission(ONE_ETH));
        given(exchangeAccount).hasOrderBooksSellOrders(ETH_BTC_SYMBOL, new ApiOrderBook.PriceAndVolume[]{
                new ApiOrderBook.PriceAndVolume(TWO, ONE),
                new ApiOrderBook.PriceAndVolume(FOUR, TWO)});
        when(manager).executesOrderDryRun(WITH_ETH_BUY_BTC, THREE_ETH);
        then(manager).validatesThatDryRunOrderReturned(BTC, firstTransactionOutcome + secondTransactionOutcome);
    }

    /**
     * I have 3 ETH
     * First entry  : i sell 2 for 4 btc, since the price is 2
     * Second entry : i sell the remaining 1 for 1 btc since price is 1
     */
    @Test
    @Ignore
    public void dryRunOrderSellAcrossTwoOrders() {
        //TODO fix commissions and test setup
        double expectedBTC = 5d - getCommission(THREE_ETH);
        given(exchangeAccount).hasOrderBookBuyOrders(ETH_BTC_SYMBOL, new ApiOrderBook.PriceAndVolume[]{
                new ApiOrderBook.PriceAndVolume(TWO, TWO),
                new ApiOrderBook.PriceAndVolume(ONE, TWO)
        });
        when(manager).executesOrderDryRun(SELL_ETH_FOR_BTC, THREE_ETH);
        then(manager).validatesThatDryRunOrderReturned(BTC, expectedBTC);
    }

    @Test
    public void dryRunOrderPickBestPriceForSell() {
    }

    @Test
    public void dryRunOrderPickBestPriceForBuy() {
    }

    @Test
    public void dryRunOrderIsAccurateIfFirstOrderCoversVolume() {
    }
    @Test
    public void dryRunOrderIsAccurateIfFirstOrderDoesNotCoverVolume() {
    }

    @Test
    public void dryRunOrderTakesFeesIntoAccount() {
        given(exchangeAccount).hasOrderBooksSellOrders(ETH_BTC_SYMBOL, oneToOneOrderBookEntryWithVolume(2));
        when(manager).executesOrderDryRun(WITH_ETH_BUY_BTC, ONE_ETH);
        then(manager).validatesThatDryRunOrderReturned(BTC, ONE - getCommission(ONE_ETH));
    }

    @Test
    public void dryRunOrderWithEmptyOrderBookThrowsNoVolumeException() {
        given(exchangeAccount).hasOrderBooksSellOrders(ETH_BTC_SYMBOL, new ApiOrderBook.PriceAndVolume[]{});
        when(manager).executesOrderDryRun(WITH_ETH_BUY_BTC, ONE_ETH);
        then(manager).validatesThatHasReceivedNotEnoughVolumeException();
    }

    @Test
    public void dryRunOnInsufficientVolumeThrowsException() {
        given(exchangeAccount).hasOrderBooksSellOrders(ETH_BTC_SYMBOL, oneToOneOrderBookEntryWithVolume(1));
        when(manager).executesOrderDryRun(WITH_ETH_BUY_BTC, TWO_ETH);
        then(manager).validatesThatHasReceivedNotEnoughVolumeException();
    }

    private ApiOrderBook.PriceAndVolume[] oneToOneOrderBookEntryWithVolume(final double volume) {
        return new ApiOrderBook.PriceAndVolume[]{
                new ApiOrderBook.PriceAndVolume(1, volume)
        };
    }

    private double getCommission(CoinAndCount coinAndCount) {
        return MathHelper.percentOf(Exchange.BINANCE.getExchangeFee(), coinAndCount.getCoinCount());
    }

    private double getCommission(double coinCount) {
        return MathHelper.percentOf(Exchange.BINANCE.getExchangeFee(), coinCount);
    }
}
