package acceptance.manages;

import com.shazam.gwen.collaborators.Actor;
import com.shazam.gwen.collaborators.Asserter;
import com.shazam.shazamcrest.MatcherAssert;
import com.shazam.shazamcrest.matcher.Matchers;
import arbitrator.exchange.BinanceManager;
import arbitrator.exchange.ExchangeManager;
import arbitrator.shared.NotEnoughCoinException;
import arbitrator.shared.NotEnoughVolumeException;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.Instrument;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;

import javax.inject.Inject;

public class BinanceManagerUser implements Actor, Asserter {
    private final ExchangeManager manager;

    private CoinAndCount lastReceivedBalance;
    private CoinAndCount lastReceivedDryRunResult;
    private Exception lastReceivedException;

    @Inject
    public BinanceManagerUser(final BinanceManager binanceManager) {
        this.manager = binanceManager;
    }

    public void reset() {
        lastReceivedBalance = null;
        lastReceivedException = null;
        lastReceivedDryRunResult = null;
    }

    public void asksForBalance(final String coin) {
        lastReceivedBalance = manager.getBalanceForCoin(coin);
    }

    public void sendsCoin(final String coin, final double coinCount) {
        try {
            manager.sendCoinToAddress(new CoinAndCount(coin, coinCount), "");
        } catch (NotEnoughCoinException e) {
            lastReceivedException = e;
        }
    }

    public void executesOrderDryRun(final Instrument instrument, final CoinAndCount coinCount) {
        try {
            lastReceivedDryRunResult = manager.getOrderExactOutput(instrument, coinCount);
        } catch (NotEnoughVolumeException e) {
            lastReceivedException = e;
        }
    }

    public void validatesThatDryRunOrderReturned(final String coin, final double coinCount) {
        MatcherAssert.assertThat(lastReceivedDryRunResult, Matchers.sameBeanAs(new CoinAndCount(coin, coinCount)));
    }

    public void validatesThatHasExactCoinCount(final String coin, final double coinCount) {
        Assert.assertEquals(coinCount, manager.getBalanceForCoin(coin).getCoinCount().doubleValue(), 0.0000001);
    }

    public void validatesThatHasReceivedNotEnoughCoinException() {
        MatcherAssert.assertThat(lastReceivedException, CoreMatchers.instanceOf(NotEnoughCoinException.class));
    }

    public void validatesThatHasReceivedNotEnoughVolumeException() {
        MatcherAssert.assertThat(lastReceivedException, CoreMatchers.instanceOf(NotEnoughVolumeException.class));
    }

    public void validatesThatHasReceived(final CoinAndCount coinAndCount) {
        MatcherAssert.assertThat(lastReceivedBalance, Matchers.sameBeanAs(coinAndCount));
    }

}
