package acceptance.setup;

import arbitrator.exchange.util.KeyConfiguration;
import arbitrator.shared.model.Exchange;

public class KeyConfigurationMock implements KeyConfiguration{

    @Override
    public String getApiKey(final Exchange exchange) {
        return "key";
    }

    @Override
    public String getApiSecret(final Exchange exchange) {
        return "secret";
    }
}
