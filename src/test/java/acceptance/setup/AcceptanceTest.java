package acceptance.setup;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.util.Modules;
import arbitrator.RuntimeModule;

public class AcceptanceTest {
    public AcceptanceTest(){
        Injector injector = Guice.createInjector(Modules.override(new RuntimeModule()).with(new TestModule()));
        injector.injectMembers(this);
    }
}


