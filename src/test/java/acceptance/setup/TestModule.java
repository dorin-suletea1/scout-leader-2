package acceptance.setup;

import acceptance.setup.api.BinanceApiForTesting;
import acceptance.setup.api.KucoinApiForTesting;
import com.google.inject.AbstractModule;
import arbitrator.exchange.bindings.BinanceApi;
import arbitrator.exchange.bindings.KucoinApi;
import arbitrator.exchange.util.KeyConfiguration;

public class TestModule extends AbstractModule{

    @Override
    protected void configure() {
        bind(KeyConfiguration.class).to(KeyConfigurationMock.class);
        bind(BinanceApi.class).to(BinanceApiForTesting.class);
        bind(KucoinApi.class).to(KucoinApiForTesting.class);
    }
}
