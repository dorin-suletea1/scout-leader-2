package acceptance.setup.api;

import arbitrator.exchange.bindings.ExchangeApi;
import arbitrator.exchange.model.ApiInstrument;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.exchange.model.ApiOrderInfo;
import arbitrator.shared.model.CoinAndCount;
import arbitrator.shared.model.CoinInfo;
import arbitrator.shared.model.KLineData;
import arbitrator.shared.model.Side;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

public abstract class ExchangeApiMock implements ExchangeApi {
    public final static String DEPOSIT_ADDRESS = "0x15";
    public final static String ORDER_ID = "orderID1";

    private List<ApiInstrument> instruments;
    private List<CoinInfo> coinInfos;
    private List<ApiOrderInfo> myActiveOrders;
    private List<CoinAndCount> myAssets;
    private Map<String, ApiOrderBook> orderBook;
    private Map<String, List<KLineData>> kLines;


    public ExchangeApiMock() {
        instruments = new ArrayList<>();
        coinInfos = new ArrayList<>();
        myActiveOrders = new ArrayList<>();
        myAssets = new ArrayList<>();
        orderBook = new HashMap<>();
        kLines = new HashMap<>();
    }

    public void reset() {
        instruments.clear();
        coinInfos.clear();
        myActiveOrders.clear();
        myAssets.clear();
        orderBook.clear();
    }

    public void removeAsset(final String coin) {
        myAssets = myAssets.stream()
                .filter(p -> coin.equals(coin))
                .collect(Collectors.toList());
    }

    public void hasInstruments(final ApiInstrument... instruments) {
        this.instruments.addAll(Arrays.asList(instruments));
    }

    public void hasKlines(final String symbol, List<KLineData> data) {
        this.kLines.put(symbol,data);
    }

    public void hasCoinInfos(final CoinInfo... info) {
        this.coinInfos.addAll(Arrays.asList(info));
    }

    public void hasActiveOrder(final ApiOrderInfo... orders) {
        this.myActiveOrders.addAll(Arrays.asList(orders));
    }

    public void hasAsset(final CoinAndCount... assets) {
        this.myAssets.addAll(Arrays.asList(assets));
    }

    public void hasOrderBook(final Pair<String, ApiOrderBook>... orders) {
        for (int i = 0; i < orders.length; i++) {
            orderBook.put(orders[i].getKey(), orders[i].getValue());
        }
    }

    @Override
    public List<ApiInstrument> getInstruments() {
        return instruments;
    }

    @Override
    public List<CoinInfo> getCoinInfos() {
        return coinInfos;
    }

    @Override
    public List<ApiOrderInfo> getMyActiveOrders(final String symbol) {
        return myActiveOrders;
    }

    @Override
    public List<CoinAndCount> getAssets() {
        return myAssets;
    }

    @Override
    public String getDepositAddress(final String coin) {
        return DEPOSIT_ADDRESS;
    }

    @Override
    public void sendCoinToAddress(final String coin, final String walletAddress, final double volume) {
        for (int i = 0; i < myAssets.size(); i++) {
            CoinAndCount assets = myAssets.get(i);
            if (assets.getCoin().equals(coin)) {
                if (assets.getCoinCount() > volume) {
                    myAssets.set(i, new CoinAndCount(assets.getCoin(), assets.getCoinCount() - volume));
                } else {
                    throw new RuntimeException("I don't have enough coin");
                }
            }
        }
    }

    @Override
    public ApiOrderBook getOrderBook(final String symbol) {
        return orderBook.get(symbol);
    }

    @Override
    public Pair<String, Double> submitLimitOrder(final String symbol, final double volume, final double price, final Side side) {
        Pair<String, String> coinPair = getLeftAndRightCoinFromSymbol(symbol, side);
        for (int i = 0; i < myAssets.size(); i++) {
            CoinAndCount assetMinus = myAssets.get(i);
            if (assetMinus.getCoin().equals(coinPair.getKey())) {
                if (assetMinus.getCoinCount() > volume) {
                    myAssets.set(i, new CoinAndCount(assetMinus.getCoin(), assetMinus.getCoinCount() - volume * price));
                    for (int j = 0; j < myAssets.size(); j++) {
                        CoinAndCount assetPlus = myAssets.get(i);
                        if (assetPlus.getCoin().equals(coinPair.getValue())) {
                            myAssets.set(j, new CoinAndCount(assetPlus.getCoin(), assetPlus.getCoinCount() + volume));
                            return new Pair<>(ORDER_ID, volume);
                        }
                    }
                    myAssets.add(new CoinAndCount(coinPair.getValue(), volume));
                }
            }
        }
        throw new RuntimeException("Bad test set-up");
    }

    @Override
    public List<KLineData> getKLineData(final String symbol, final int interval, final int limit) {
        return kLines.get(symbol);
    }

    public Pair<String, String> getLeftAndRightCoinFromSymbol(final String symbol, Side side) {
        final String rightSymbol = symbol.substring(0, symbol.length() - 3);
        final String leftSymbol = symbol.substring(symbol.length() - 3, symbol.length());
        return side == Side.BUY ? new Pair<>(rightSymbol, leftSymbol) : new Pair<>(leftSymbol, rightSymbol);
    }
}
