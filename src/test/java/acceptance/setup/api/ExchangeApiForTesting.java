package acceptance.setup.api;

import com.shazam.gwen.collaborators.Arranger;
import arbitrator.exchange.model.ApiOrderBook;
import arbitrator.shared.model.CoinAndCount;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public abstract class ExchangeApiForTesting extends ExchangeApiMock implements Arranger {
    public void doesNotHaveCoinBalance(final String coin) {
        super.removeAsset(coin);
    }

    public void hasCoinBalance(final String coin, final double balance) {
        super.hasAsset(new CoinAndCount(coin, balance));
    }

    public void hasOrderBookBuyOrders(final String symbol, ApiOrderBook.PriceAndVolume... priceAndVolume) {
        updateOrderBookWith(symbol, Arrays.asList(priceAndVolume), Collections.emptyList());
    }

    public void hasOrderBooksSellOrders(final String symbol, ApiOrderBook.PriceAndVolume... priceAndVolume) {
        updateOrderBookWith(symbol, Collections.emptyList(), Arrays.asList(priceAndVolume));
    }

    public void hasOrderBooksSellOrders(Pair<String, ApiOrderBook.PriceAndVolume> symbolAndSellOrderBookEntries) {
        updateOrderBookWith(symbolAndSellOrderBookEntries.getKey(), Collections.emptyList(), Collections.singletonList(symbolAndSellOrderBookEntries.getValue()));
    }

    private void updateOrderBookWith(final String symbol, final List<ApiOrderBook.PriceAndVolume> demandUpdates, final List<ApiOrderBook.PriceAndVolume> offerUpdates) {
        ApiOrderBook oldOrderBook = super.getOrderBook(symbol);
        if (oldOrderBook == null) {
            oldOrderBook = new ApiOrderBook(Collections.emptyList(), Collections.emptyList());
        }
        List<ApiOrderBook.PriceAndVolume> newOffer = new ArrayList<>();
        newOffer.addAll(oldOrderBook.getMarketOffer());
        newOffer.addAll(offerUpdates);
        List<ApiOrderBook.PriceAndVolume> newDemand = new ArrayList<>();
        newDemand.addAll(oldOrderBook.getMarketDemand());
        newDemand.addAll(demandUpdates);
        super.hasOrderBook(new Pair<>(symbol, new ApiOrderBook(newDemand, newOffer)));
    }
}
