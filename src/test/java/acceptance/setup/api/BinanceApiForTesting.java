package acceptance.setup.api;

import com.google.inject.Singleton;
import arbitrator.exchange.bindings.BinanceApi;
import arbitrator.shared.model.Exchange;

@Singleton
public class BinanceApiForTesting extends ExchangeApiForTesting implements BinanceApi {
    @Override
    public Exchange getExchange() {
        return Exchange.BINANCE;
    }
}