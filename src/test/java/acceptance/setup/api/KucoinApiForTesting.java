package acceptance.setup.api;

import com.google.inject.Singleton;
import arbitrator.exchange.bindings.KucoinApi;
import arbitrator.shared.model.Exchange;

@Singleton
public class KucoinApiForTesting extends ExchangeApiForTesting implements KucoinApi {
    @Override
    public Exchange getExchange() {
        return Exchange.KUCOIN;
    }
}