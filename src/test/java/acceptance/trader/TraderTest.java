package acceptance.trader;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class TraderTest {
    @Test
    public void preCalculatedOutputIsExact(){
    }

    @Test
    public void operationsAreTraverseable(){
        //trade1.right == trade2.left; trade2.right == trade3.left etc
    }

    @Test
    public void operationsDoNotOverlap(){
        //2 operations do not happen at the same time, they wait for the previous to finish
    }

    @Test
    public void willNotStartOnNotEnoughVolume(){
        //before starting a trade chain, i will check the volume
    }

    @Test
    public void willNotStartOnNotProfitable(){
    }

    @Test
    public void exitsOnBecomesNotProfitable(){
    }

    @Test
    public void exitsOnBecomesNotEnoughVolume(){
    }
}
